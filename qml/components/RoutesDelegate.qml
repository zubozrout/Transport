import QtQuick 2.9
import Lomiri.Components 1.3
import QtQuick.Layouts 1.1
import QtGraphicalEffects 1.0

import "../generalfunctions.js" as GeneralFunctions
import "../transport-api.js" as Transport

Component {
    id: routesDelegate

    Item {
        anchors {
            left: parent.left
            right: parent.right
            topMargin: units.gu(1)
            bottomMargin: units.gu(1)
        }
        height: childrenRect.height

        ColumnLayout {
            anchors {
                left: parent.left
                right: parent.right
            }
            spacing: units.gu(0.5)

            RowLayout {
                id: routeRow
                spacing: units.gu(2)
                Layout.fillWidth: true

                ColumnLayout {
                    Layout.fillWidth: false
                    Layout.fillHeight: true
                    Layout.minimumWidth: units.gu(4)
                    Layout.preferredWidth: units.gu(4)
                    spacing: units.gu(0.05)
                    
                    TransportIcon {
                        id: transportTypeIconItem
                        Component.onCompleted: {
                            setData(typeIndex, num);
                        }
                    }

                    Label {
                        text: num
                        fontSizeMode: Text.Fit
                        font.pixelSize: FontUtils.sizeToPixels("large")
                        font.bold: true
                        horizontalAlignment: Text.AlignHCenter
                        wrapMode: Text.WordWrap

                        Layout.fillWidth: true
                        Layout.alignment: Qt.AlignCenter
                    }
                }

                GridLayout {
                    id: gridLayout
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    //Layout.maximumWidth: units.gu(20)
                    columns: 2
                    rowSpacing: units.gu(0.25)
                    clip: true

                    RowLayout {
                        spacing: units.gu(1)
                        Layout.fillWidth: true
                        Layout.fillHeight: true
                        Layout.alignment: Qt.AlignLeft

                        Label {
                            text: from.name || ""
                            font.pixelSize: FontUtils.sizeToPixels("normal")
                            font.bold: false
                            wrapMode: Text.WordWrap

                            Layout.fillWidth: true
                            Layout.maximumWidth: fullWidth
                            Layout.minimumWidth: units.gu(5)
                            Layout.alignment: Qt.AlignLeft

                            property var fullWidth: 0
                            onWidthChanged: {
                                const previousWrapMode = wrapMode;
                                wrapMode = Text.NoWrap;
                                fullWidth = contentWidth + units.gu(1);
                                wrapMode = previousWrapMode;
                            }
                        }

                        PlatformLabel {
                            platform: from.platform
                        }
                    }

                    RowLayout {
                        spacing: units.gu(1)
                        Layout.fillWidth: true
                        Layout.fillHeight: true
                        Layout.alignment: Qt.AlignRight

                        Item {
                            Layout.fillWidth: true
                        }

                        Label {
                            id: delayLabel
                            text: delay ? i18n.tr("%1 minute delay", "%1 minutes delay", delay).arg(delay) : ""
                            font.pixelSize: FontUtils.sizeToPixels("normal")
                            font.bold: true
                            color: upToDate ? pageLayout.colorPalete["warningText"] : pageLayout.colorPalete["secondaryText"]
                            horizontalAlignment: Text.AlignRight
                            wrapMode: Text.WordWrap
                            visible: text !== ""
                            
                            property bool upToDate: true

                            Layout.fillWidth: true
                            Layout.alignment: Qt.AlignLeft
                        }

                        Label {
                            text: from.time || ""
                            font.pixelSize: FontUtils.sizeToPixels("normal")
                            font.bold: false
                            wrapMode: Text.NoWrap
                            horizontalAlignment: Text.AlignRight

                            Layout.fillWidth: true
                            Layout.maximumWidth: contentWidth
                            Layout.alignment: Qt.AlignRight
                        }
                    }

                    RowLayout {
                        spacing: units.gu(1)
                        Layout.fillWidth: true
                        Layout.fillHeight: true
                        Layout.alignment: Qt.AlignLeft

                        Label {
                            text: to.name || ""
                            font.pixelSize: FontUtils.sizeToPixels("normal")
                            font.bold: false
                            wrapMode: Text.WordWrap

                            Layout.fillWidth: true
                            Layout.maximumWidth: fullWidth
                            Layout.minimumWidth: units.gu(5)
                            Layout.alignment: Qt.AlignLeft

                            property var fullWidth: 0
                            onWidthChanged: {
                                const previousWrapMode = wrapMode;
                                wrapMode = Text.NoWrap;
                                fullWidth = contentWidth + units.gu(1);
                                wrapMode = previousWrapMode;
                            }
                        }

                        PlatformLabel {
                            platform: to.platform
                        }
                    }

                    RowLayout {
                        spacing: units.gu(1)
                        Layout.fillWidth: true
                        Layout.fillHeight: true
                        Layout.alignment: Qt.AlignRight

                        Item {
                            Layout.fillWidth: true
                        }

                        Label {
                            text: to.time || ""
                            font.pixelSize: FontUtils.sizeToPixels("normal")
                            font.bold: false
                            wrapMode: Text.NoWrap
                            horizontalAlignment: Text.AlignRight

                            Layout.fillWidth: true
                            Layout.maximumWidth: contentWidth
                            Layout.alignment: Qt.AlignRight
                        }
                    }
                }
            }

            Label {
                text: {
                    const fixedCodesData = fixedCodes ? JSON.parse(fixedCodes) : [];
                    return fixedCodesData ? fixedCodesData.filter(function(data) {
                        return data.text === "!";
                    }).map(function(data) {
                        return data.desc;
                    }).join("\n") : "";
                }
                font.pixelSize: FontUtils.sizeToPixels("small")
                font.bold: false
                horizontalAlignment: Text.AlignLeft
                wrapMode: Text.WordWrap
                visible: text

                Layout.fillWidth: true
            }
        }
        
        DelayTimer {
            id: delayTimer
            enabled: true
            conId: connectionsID
            query: typeof delayQuery !== typeof undefined ? delayQuery : null
            callback: function(response) {
                response = response || {};
                if(typeof response.delay !== typeof undefined) {
                    delay = response.delay;
                    delayLabel.upToDate = true;
                }
                else {
                    delayLabel.upToDate = false;
                }
            }
        }
    }
}
