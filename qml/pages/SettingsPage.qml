import QtQuick 2.9
import Lomiri.Components 1.3

import "../components"

import "../transport-api.js" as Transport

Page {
    id: settingsPage
    header: PageHeader {
        id: pageHeader
        title: i18n.tr("Settings")
        flickable: settingsFlickable

        StyleHints {
            foregroundColor: pageLayout.colorPalete["headerText"]
            backgroundColor: pageLayout.colorPalete["headerBG"]
        }
    }
    
    clip: true
    
    Flickable {
        id: settingsFlickable
        anchors.fill: parent
        contentHeight: settingsFlickableRectangle.height
        flickableDirection: Flickable.VerticalFlick

        Item {
            id: settingsFlickableRectangle
            anchors {
                left: parent.left
                right: parent.right
                margins: units.gu(1)
            }
            height: childrenRect.height + 2 * anchors.margins

            Column {
                id: aboutColumn
                anchors {
                    left: parent.left
                    right: parent.right
                }
                spacing: units.gu(2)

                CustomDataList {
                    id: customDataList
                    
                    Component.onCompleted: {
                        customDataList.append({
                            value: i18n.tr("Clear cache"),
                            fontScale: "large",
                            bottomBorder: false
                        });

                        customDataList.setCallbackFor(customDataList.count(), function() {
                            Transport.transportOptions.clearAll(true);
                            rowPickerA.update(function(model) { rowPickerA.render(model) });
                            rowPickerB.update(function(model) { rowPickerB.render(model) });
                            rowPickerC.update(function(model) { rowPickerC.render(model) });
                            rowPickerD.update(function(model) { rowPickerD.render(model) });
                        });

                        customDataList.append({
                            value: i18n.tr("By pressing this button you'll delete all saved data including search history, cached stations and respective transport option settings."),
                            buttonIcon: "delete",
                            bottomBorder: true
                        });

                        customDataList.append({
                            value: i18n.tr("Refresh data"),
                            fontScale: "large",
                            bottomBorder: false
                        });

                        customDataList.setCallbackFor(customDataList.count(), function() {
                            transportSelectorPage.serverUpdate();
                            rowPickerA.update(function(model) { rowPickerA.render(model) });
                            rowPickerB.update(function(model) { rowPickerB.render(model) });
                            rowPickerC.update(function(model) { rowPickerC.render(model) });
                            rowPickerD.update(function(model) { rowPickerD.render(model) });
                        });

                        customDataList.append({
                            value: i18n.tr("Renew all Transport options data"),
                            buttonIcon: "reload",
                            bottomBorder: true
                        });
                    }
                }
                
                RowPicker {
                    id: rowPickerA

                    property var render: function(model) {
                        clear();

                        var options = [i18n.tr("Yes"), i18n.tr("No")];
                                                
                        var index = Number(Transport.transportOptions.getDBSetting("geolocation-on-start") || 0)

                        initialize(options, index, function(itemIndex) {
                            Transport.transportOptions.saveDBSetting("geolocation-on-start", itemIndex);
                        });
                    }

                    Component.onCompleted: {
                        setTite(i18n.tr("Use GeoLocation search on start?"));
                        setFootNote(i18n.tr("Allow nearby station search on start. Unless online source provider is selected below your device coordinates are not sent online."));
                        rowPickerA.update(function(model) { rowPickerA.render(model) });
                    }
                }
                
                RowPicker {
                    id: rowPickerB

                    property var render: function(model) {
                        clear();

                        var options = [i18n.tr("Online"), i18n.tr("Local only")];
                                                
                        var index = Number(Transport.transportOptions.getDBSetting("geolocation-source") || 0);

                        initialize(options, index, function(itemIndex) {
                            Transport.transportOptions.saveDBSetting("geolocation-source", itemIndex);
                        });
                    }

                    Component.onCompleted: {
                        setTite(i18n.tr("GeoLocation search source"));
                        setFootNote(i18n.tr("Select data source to check the current device location against. Local data are limited and only contain stations ever searched for."));
                        rowPickerB.update(function(model) { rowPickerB.render(model) });
                    }
                }
                
                RowPicker {
                    id: rowPickerC

                    property var render: function(model) {
                        clear();

                        var options = [i18n.tr("Yes"), i18n.tr("No")];
                                                
                        var index = Number(Transport.transportOptions.getDBSetting("connection-detail-coords") || 0);

                        initialize(options, index, function(itemIndex) {
                            Transport.transportOptions.saveDBSetting("connection-detail-coords", itemIndex);
                        });
                    }

                    Component.onCompleted: {
                        setTite(i18n.tr("Download route map coords with every connection detail query"));
                        setFootNote(i18n.tr("If turned off you won't be able to see selected route on the map."));
                        rowPickerC.update(function(model) { rowPickerC.render(model) });
                    }
                }

                RowPicker {
                    id: rowPickerD

                    property var render: function(model) {
                        clear();

                        var options = [i18n.tr("Weekly"), i18n.tr("Daily"), i18n.tr("Everytime"), i18n.tr("Never")];
                        var index = Number(Transport.transportOptions.getDBSetting("check-frequency") || 0);

                        initialize(options, index, function(itemIndex) {
                            Transport.transportOptions.saveDBSetting("check-frequency", itemIndex);
                        });
                    }

                    Component.onCompleted: {
                        setTite(i18n.tr("How often would you like the transport options data to be refreshed?"));
                        rowPickerD.update(function(model) { rowPickerD.render(model) });
                    }
                }
                
                RowPicker {
                    id: rowPickerE

                    property var render: function(model) {
                        clear();

                        var options = [i18n.tr("Yes"), i18n.tr("No")];
                                                
                        var index = Number(Transport.transportOptions.getDBSetting("update-delays") || 0);

                        initialize(options, index, function(itemIndex) {
                            Transport.transportOptions.saveDBSetting("update-delays", itemIndex);
                        });
                    }

                    Component.onCompleted: {
                        setTite(i18n.tr("Update delays for displayed connections"));
                        setFootNote(i18n.tr("If disabled the delay shown will not be updated periodically but will instead always match the data fetched with the initial search. To refresh a new search will be necessary."));
                        rowPickerE.update(function(model) { rowPickerE.render(model) });
                    }
                }

                RowPicker {
                    id: rowPickerF

                    property var render: function(model) {
                        clear();

                        var options = [i18n.tr("No"), i18n.tr("Yes")];
                                                
                        var index = Number(Transport.transportOptions.getDBSetting("sort-connections-delay") || 0);

                        initialize(options, index, function(itemIndex) {
                            Transport.transportOptions.saveDBSetting("sort-connections-delay", itemIndex);
                        });
                    }

                    Component.onCompleted: {
                        setTite(i18n.tr("Sort connections taking delays into account"));
                        setFootNote(i18n.tr("If disabled the connections will be sorted by scheduled departure time. If enabled delays will be taken into account for connections ordering."));
                        rowPickerF.update(function(model) { rowPickerF.render(model) });
                    }
                }
                
                CustomDataList {
                    id: customDataListBottom
                    
                    Component.onCompleted: {
                        customDataListBottom.setCallbackFor(customDataList.count(), function() {
                            pageLayout.addPageToNextColumn(searchPage, welcomePage);
                            Transport.transportOptions.saveDBSetting("not-first-run", 0);
                        }, false);

                        customDataListBottom.append({
                            value: i18n.tr("Display Welcome screen"),
                            buttonIcon: "help",
                            topBorder: true
                        });
                    }
                }
            }
        }
    }

    Scrollbar {
        flickableItem: settingsFlickable
        align: Qt.AlignTrailing
    }
}
