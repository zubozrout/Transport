import QtQuick 2.9
import Lomiri.Components 1.3
import Lomiri.Components.Pickers 1.0

import "../transport-api.js" as Transport

Rectangle {
    id: dateTimePicker
    anchors {
        left: parent.left
        right: parent.right
    }
    height: dateTimePickerColumn.height + 2 * dateTimePickerColumn.anchors.margins
    color: pageLayout.colorPalete["baseBG"]
    
    property var pickerHeight: units.gu(14)
    property var datePicker: dateYMDPicker
    property var timePicker: dateHMPicker
    
    Column {
        id: dateTimePickerColumn
        anchors {
            left: parent.left
            right: parent.right
            margins: units.gu(2)
            verticalCenter: parent.verticalCenter
        }
        spacing: units.gu(1)

        DatePicker {
            id: dateYMDPicker
            anchors.horizontalCenter: parent.horizontalCenter
            height: datetimePicker.pickerHeight
            mode: "Years|Months|Days"
            date: new Date()

            property bool customDateSet: false

            onVisibleChanged: {
                if(!customDateSet) {
                    var selectedTransport = Transport.transportOptions.getSelectedTransport();
                    if(selectedTransport) {
                        minimum = selectedTransport.getValidity().from;
                        maximum = selectedTransport.getValidity().to;

                        var currentDate = new Date();
                        if(minimum <= currentDate <= maximum) {
                            date = currentDate;
                        }

                        customDateSet = true;
                    }
                }
            }
        }

        DatePicker {
            id: dateHMPicker
            anchors.horizontalCenter: parent.horizontalCenter
            height: datetimePicker.pickerHeight
            mode: "Hours|Minutes"
            date: new Date()
        }

        Label {
            text: i18n.tr("Selected date:") + " " + Qt.formatDate(dateYMDPicker.date, "dd.MM.yyyy") + ", " + Qt.formatTime(dateHMPicker.date, "hh:mm")
            width: parent.width
            font.pixelSize: FontUtils.sizeToPixels("normal")
            horizontalAlignment: Text.AlignHCenter
            wrapMode: Text.WordWrap
            color: pageLayout.colorPalete.baseText
        }
    }
}
