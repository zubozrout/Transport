#!/bin/bash

# Switch to current directory
cd "${0%/*}"
# Delete build data
clickable clean
# Build the project
clickable build
clickable build --arch arm64
clickable build --arch amd64
# Review the final package
clickable review
