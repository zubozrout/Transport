"use strict";

var Stop = function(data, parentData) {
    this.parentData = parentData || {};
    this.dbConnection = this.parentData.dbConnection || null;
    
    if(typeof data === typeof "string") {
        this.basic = true;
        this.value = data;
        this.transportID = this.parentData.transportID || null;
    }
    else {
        this.basic = false;
        this.data = data || {};
        this.id = this.data.id !== undefined ? Number(this.data.id) : -1;
        this.data.item = this.data.item || {};
        this.transportID = this.parentData.transportID || this.data.key || null;
    }
    return this;
}

Stop.prototype.getName = function() {
    if(!this.basic) {
        return this.data.item.name || null;
    }
    return this.value || null;
}

Stop.prototype.getId = function() {
    if(!this.basic && this.id !== null) {
        return Number(this.id);
    }
    return null;
}

Stop.prototype.getListId = function() {
    if(!this.basic && this.data.item.listId !== undefined && this.data.item.listId !== null) {
        return Number(this.data.item.listId);
    }
    return null;
}

Stop.prototype.getItem = function() {
    if(!this.basic && this.data.item.item !== undefined) {
        return Number(this.data.item.item);
    }
    return null;
}

Stop.prototype.getTransportId = function(item) {
    return this.transportID;
}

Stop.prototype.setId = function(id) {
    if(id !== undefined) {
        this.id = Number(id);
    }
}

Stop.prototype.setItem = function(item) {
    if(!this.basic) {
        this.data.item.item = item;
        return true;
    }
    return false;
}

Stop.prototype.getCoor = function() {
    const coor = this.data ? {
        coorX: this.data.coorX || (this.data.item ? this.data.item.coorX : null),
        coorY: this.data.coorY || (this.data.item ? this.data.item.coorY : null)
    } : {}
    if(coor.coorX && coor.coorY) {
        return coor;
    }
    return null;
}

Stop.prototype.saveToDB = function() {
    if(!this.basic && this.transportID && this.dbConnection) {
        const options = {
            value: this.getName(),
            item: this.getItem(), // index under listId
            listId: this.getListId() // ID of object list
        };

        const coors = this.getCoor();
        if(coors) {
            options.coorX = coors.coorX;
            options.coorY = coors.coorY;
        }

        this.id = this.dbConnection.saveStation(this.transportID, options);
        console.log("Station attempted to be saved to DB:", this.getName(), this.id);
        return this.id;
    }
    return false;
}
