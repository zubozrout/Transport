import QtQuick 2.9
import Lomiri.Components 1.3
import QtQuick.Layouts 1.1
import Lomiri.Components.Popups 1.0

import "../transport-api.js" as Transport
import "../generalfunctions.js" as GeneralFunctions

BottomEdge {
    id: bottomEdge
    height: parent.height
    hint.text: i18n.tr("Recent searches")

    Component.onCompleted: {
        QuickUtils.mouseAttached = true;
    }

    contentComponent: Item {
        width: bottomEdge.width
        height: bottomEdge.height
        opacity: bottomEdge.dragProgress

        PageHeader {
            id: recentPageHeader
            title: i18n.tr("Recent searches")

            trailingActionBar {
                actions: [
                    Action {
                        iconName: "delete"
                        text: i18n.tr("Delete all searches")
                        onTriggered: PopupUtils.open(confirmDeletingAllHistory)
                        enabled: recentListModel.count > 0
                        visible: enabled
                    }
                ]
            }

            StyleHints {
                foregroundColor: pageLayout.colorPalete["headerText"]
                backgroundColor: pageLayout.colorPalete["headerBG"]
            }
        }
        
        Timer {
            id: removeItemFromListTimer
            interval: 250
            repeat: false
            running: false
            triggeredOnStart: false
            
            property var itemIndex: -1

            onTriggered: {
                if(itemIndex >= 0 && recentListModel.count > 0 && itemIndex < recentListModel.count) {
                    recentListModel.remove(itemIndex);
                }
            }
        }

        Component {
            id: confirmDeletingAllHistory

            Dialog {
                id: confirmDeletingAllHistoryDialogue
                title: i18n.tr("Attention")
                text: i18n.tr("Do you really want to delete the whole search history?")
                Button {
                    text: i18n.tr("No")
                    onClicked: PopupUtils.close(confirmDeletingAllHistoryDialogue)
                }
                Button {
                    text: i18n.tr("Yes")
                    color: LomiriColors.red
                    onClicked: {
                        Transport.transportOptions.dbConnection.deleteAllSearchHistory();
                        PopupUtils.close(confirmDeletingAllHistoryDialogue);
                        bottomEdge.collapse();
                    }
                }
            }
        }

        Component {
            id: recentChildDelegate

            ListItem {
                id: recentListItem
                width: parent.width
                divider.visible: true
                height: recentChildDelegateLayoutWrapper.height
                
                Component {
                    id: confirmDeletingHistoryEntry

                    Dialog {
                        id: confirmDeletingHistoryEntryDialogue
                        title: i18n.tr("Attention")
                        text: i18n.tr("Do you really want to delete this history entry?")
                        
                        Button {
                            text: i18n.tr("No")
                            onClicked: PopupUtils.close(confirmDeletingHistoryEntryDialogue)
                        }
                        
                        Button {
                            text: i18n.tr("Yes")
                            color: LomiriColors.red
                            onClicked: {
                                Transport.transportOptions.dbConnection.deleteSearchHistory(ID);
                                removeItemFromListTimer.itemIndex = index;
                                removeItemFromListTimer.start();
                                PopupUtils.close(confirmDeletingHistoryEntryDialogue);
                            }
                        }
                    }
                }

                leadingActions: ListItemActions {
                    actions: [
                        Action {
                            iconName: "delete"
                            enabled: true
                            visible: true
                            onTriggered: {
                                PopupUtils.open(confirmDeletingHistoryEntry);
                            }
                        }
                    ]
                }
                
                Item {
                    id: recentChildDelegateLayoutWrapper
                    anchors {
                        left: parent.left
                        right: parent.right
                    }
                    height: recentChildDelegateLayout.height + recentChildDelegateLayout.anchors.topMargin + recentChildDelegateLayout.anchors.bottomMargin
                    
                    RowLayout {
                        id: recentChildDelegateLayout
                        anchors {
                            centerIn: parent
                            topMargin: units.gu(1)
                            rightMargin: units.gu(2)
                            bottomMargin: units.gu(1)
                            leftMargin: units.gu(2)
                        }
                        spacing: units.gu(2)
                        Layout.fillWidth: true
                        width: parent.width - anchors.leftMargin - anchors.rightMargin

                        Rectangle {
                            id: recentChildDelegateIndex
                            width: units.gu(2)
                            height: width
                            color: "transparent"

                            Label {
                                anchors.fill: parent
                                horizontalAlignment: Text.AlignLeft
                                verticalAlignment: Text.AlignVCenter
                                text: (index + 1) + "."
                                font.pixelSize: FontUtils.sizeToPixels("medium")
                                font.bold: true
                                wrapMode: Text.WordWrap
                                color: Transport.transportOptions.getDBSetting(key) || pageLayout.colorPalete["baseText"]
                            }
                        }

                        ColumnLayout {
                            id: historyMainColumnLayout
                            spacing: units.gu(0.25)
                            Layout.fillWidth: true

                            RowLayout {
                                spacing: units.gu(2)
                                Layout.fillWidth: true

                                Label {
                                    text: {
                                        var langCode = Transport.langCode(true);
                                        return Transport.transportOptions.getTransportById(key).getName(langCode);
                                    }
                                    font.pixelSize: FontUtils.sizeToPixels("small")
                                    font.bold: true
                                    horizontalAlignment: Text.AlignLeft
                                    wrapMode: Text.WordWrap
                                    color: Transport.transportOptions.getDBSetting(key) || pageLayout.colorPalete["baseText"]
                                    Layout.fillWidth: true
                                }

                                Label {
                                    text: GeneralFunctions.dateToString(new Date(date.replace(/-/g, "/")))
                                    font.pixelSize: FontUtils.sizeToPixels("small")
                                    horizontalAlignment: Text.AlignRight
                                    wrapMode: Text.WordWrap
                                    Layout.fillWidth: true
                                }
                            }

                            RowLayout {
                                id: historyRowLayout
                                spacing: units.gu(1)
                                Layout.fillWidth: true
                                    
                                ColumnLayout {
                                    id: fromStationColumn
                                    spacing: units.gu(0.25)
                                    Layout.preferredWidth: recentChildDelegateLayout.width / 2 - recentChildDelegateIndex.width / 2
                                    Layout.fillWidth: false
                                    
                                    Label {
                                        text: i18n.tr("From")
                                        font.pixelSize: FontUtils.sizeToPixels("small")
                                        font.bold: false
                                        horizontalAlignment: Text.AlignLeft
                                        Layout.fillWidth: true
                                        wrapMode: Text.WordWrap
                                    }

                                    Label {
                                        text: stopnamefrom
                                        font.pixelSize: FontUtils.sizeToPixels("normal")
                                        font.bold: true
                                        horizontalAlignment: Text.AlignLeft
                                        Layout.fillWidth: true
                                        wrapMode: Text.WordWrap
                                    }
                                }
                                    
                                ColumnLayout {
                                    id: toStationColumn
                                    spacing: units.gu(0.25)
                                    Layout.fillWidth: true
                                    
                                    Label {
                                        text: i18n.tr("To")
                                        font.pixelSize: FontUtils.sizeToPixels("small")
                                        font.bold: false
                                        horizontalAlignment: Text.AlignLeft
                                        Layout.fillWidth: true
                                        wrapMode: Text.WordWrap
                                    }

                                    Label {
                                        text: stopnameto
                                        font.pixelSize: FontUtils.sizeToPixels("normal")
                                        font.bold: true
                                        horizontalAlignment: Text.AlignLeft
                                        Layout.fillWidth: true
                                        wrapMode: Text.WordWrap
                                    }
                                }
                            }

                            Label {
                                text: visible ? i18n.tr("Via") + ": " + stopnamevia : ""
                                font.pixelSize: FontUtils.sizeToPixels("small")
                                Layout.fillWidth: true
                                horizontalAlignment: Text.AlignLeft
                                wrapMode: Text.WordWrap
                                visible: typeof stopidvia !== typeof undefined && stopidvia >= 0 && stopnamevia ? true : false
                            }
                        }
                    }
                }

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        Transport.support.selectStations({
                            TransportStop: Transport.Stop,
                            dbConnection: Transport.transportOptions.dbConnection,
                            selectedTransport: Transport.transportOptions.selectTransportById(key),
                            stopidfrom: stopidfrom || null,
                            stopnamefrom: stopnamefrom || null,
                            stopidto: stopidto || null,
                            stopnameto: stopnameto || null,
                            stopidvia: stopidvia || null,
                            stopnamevia: stopnamevia || null
                        });
                        
                        rowSwitchLayout.currentIndex = 0;
                        bottomEdge.collapse();
                    }
                }
            }
        }

        ListView {
            id: recentListView
            anchors.top: recentPageHeader.bottom
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            clip: true
            
            removeDisplaced: Transition {
                NumberAnimation { properties: "x,y"; duration: 250 }
            }

            model: ListModel {
                id: recentListModel

                onCountChanged: {
                    bottomEdge.visible = count > 0 ? true : false;
                }
            }
            delegate: recentChildDelegate

            function refresh() {
                var modelData = Transport.transportOptions.dbConnection.getSearchHistory();
                model.clear();
                for(var i = 0; i < modelData.length; i++) {
                    modelData[i].stopnamevia = modelData[i].stopnamevia || ""; // Hack for ListModel to always register this key
                    model.append(modelData[i]);
                }
            }

            Component.onCompleted: {
                refresh();
            }

            onVisibleChanged: {
                if(visible) {
                    refresh();
                }
            }

            Scrollbar {
                flickableItem: recentListView
                align: Qt.AlignTrailing
            }
        }
    }
}
