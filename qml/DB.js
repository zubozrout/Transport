"use strict";

var DBConnection = function(version) {
    this.name = "transport-basic";
    this.version = version || "0.1";
    this.fullName = "CZ+SK-transport-app";
    this.size = 100000;
    
    console.log("DB version: " + this.version);
    
    this.enabled = false;
    
    this.db = null;
    this.open();
}

DBConnection.prototype.open = function() {
    if(this.db === null) {
        try {
            var self = this;
            this.db = Sql.LocalStorage.openDatabaseSync(this.name, "", this.fullName, this.size);
            
            if(this.db) {
                if(!this.db.version || Number(this.db.version) !== Number(this.version)) {
                    console.log("Changing DB version:", this.db.version, this.version);
                    this.enabled = true;
                    
                    this.getAllData(this.db, function(data) {
                        self.enabled = false;
                        self.dropTables();
                        self.createTables(function() {
                            self.db.changeVersion(self.db.version, self.version, function(tx) {
                                self.migrate(tx, data);
                                self.enabled = true;
                                self.runCallbacks();
                            });
                        });                        
                    });
                }
                else {
                    console.log("Keeping old DB version", this.db.version, "(" + this.version + " expected)");
                    this.createTables(function() {
                        self.enabled = true;
                        self.runCallbacks();
                    });
                }
            }
        } catch(err) {
            console.log("Error opening database: " + err);
        }
    }

    return this;
}


/* runFunctionOnLoad */
DBConnection.prototype.onLoad = function(callback) {
    if(this.enabled) {
        callback(this);
    }
    else {
        this.callbacks = this.callbacks || [];
        this.callbacks.push(callback);
    }
}

/* runFunctionOnLoad */
DBConnection.prototype.runCallbacks = function() {
    if(this.callbacks) {
        for(var i = 0; i < this.callbacks.length; i++) {
            this.callbacks[i](this);
        }
    }
}

/* Old data migration where applicable */
DBConnection.prototype.getAllData = function(db, callback) {
    if(db) {
        var data = {};
        db.transaction(function(tx) {
            var settings = tx.executeSql("SELECT name FROM sqlite_master WHERE type='table' AND name='settings'");
            var datajson = tx.executeSql("SELECT name FROM sqlite_master WHERE type='table' AND name='datajson'");
            var stops = tx.executeSql("SELECT name FROM sqlite_master WHERE type='table' AND name='stops'");
            var recent = tx.executeSql("SELECT name FROM sqlite_master WHERE type='table' AND name='recent'");
            data = {
                settings: settings.rows.length ? tx.executeSql("SELECT * FROM settings") : null,
                datajson: datajson.rows.length ? tx.executeSql("SELECT * FROM datajson") : null,
                stops: stops.rows.length ? tx.executeSql("SELECT * FROM stops") : null,
                recent: recent.rows.length ? tx.executeSql("SELECT * FROM recent") : null
            };
            callback && callback(data);
        });
        return data;
    }
    return null;
}

/* Fill table with passed data */
DBConnection.prototype.fillTableWithData = function(tx, tableName, data) {
    var errorOccured = false;
    if(tx && tableName && data) {
        console.log("Migrating data to table", tableName);
        for(var i = 0; i < data.rs.rows.length; i++) {
            var itemData = data.rs.rows.item(i);
            var keys = Object.keys(itemData);
            var values = Object.values(itemData);
            var lines = tx.executeSql("INSERT INTO " + tableName + " VALUES(" + keys.join(",") + ")", values.join(","));
            if(lines <= 0) {
                errorOccured = true;
                console.log("Failed inserting old data into a new table", tableName, "on migration.", JSON.stringify(keys), JSON.stringify(values));
            }
            else {
                console.log("Inserted old data into a new table", tableName, "on migration.", JSON.stringify(keys), JSON.stringify(values));
            }
        }
    }
    return errorOccured;
}

/* Old data migration where applicable */
DBConnection.prototype.migrate = function(tx, data) {
    var databaseNames = Object.keys(data);
    for(var i = 0; i < databaseNames.length; i++) {
        this.fillTableWithData(tx, databaseNames[i], data[databaseNames[i]]);
    }
}

DBConnection.prototype.dropTables = function() {
    console.log("dropTables");
    if(this.enabled && this.db) {
        var self = this;
        this.db.transaction(function(tx) {
            tx.executeSql("DROP TABLE settings");
            tx.executeSql("DROP TABLE datajson");
            tx.executeSql("DROP TABLE stops");
            tx.executeSql("DROP TABLE recent");
            self.createTables();
        });
    }
}

DBConnection.prototype.createTables = function(callback) {
    if(this.db) {
        var self = this;
        try {
            this.db.transaction(function(tx) {
                var createTable = function(string) {
                    console.log("Table structure:", string);
                    tx.executeSql(string);
                }
                
                createTable("CREATE TABLE IF NOT EXISTS settings(" + [
                    "key TEXT UNIQUE",
                    "value TEXT"
                ].join(", ") + ")");
                
                createTable("CREATE TABLE IF NOT EXISTS datajson(" + [
                    "key TEXT UNIQUE",
                    "date DATETIME DEFAULT CURRENT_TIMESTAMP",
                    "value TEXT"
                ].join(", ") + ")");
                
                createTable("CREATE TABLE IF NOT EXISTS stops(" + [
                    "ID INTEGER PRIMARY KEY AUTOINCREMENT", // Unique stop id - index as saved to the database
                    "key TEXT", // Transport type unique key
                    "item INTEGER", // Unreliable, changes very often
                    "listId INTEGER", // Unreliable, changes very often
                    "value TEXT", // Stop name
                    "coorX REAL", // Geoposition X
                    "coorY REAL" // Geoposition Y
                ].join(", ") + ", UNIQUE (ID, key, item, listId) ON CONFLICT REPLACE)");
                
                createTable("CREATE TABLE IF NOT EXISTS recent(" + [
                    "ID INTEGER PRIMARY KEY AUTOINCREMENT", // Unique history item id - index as saved to the database
                    "date DATETIME DEFAULT CURRENT_TIMESTAMP", // Datestamp of saving the item to search history
                    "key TEXT", // Transport type unique key
                    "stopidfrom INTEGER", // Un ique stop id
                    "stopidto INTEGER", // Unique stop id
                    "stopidvia INTEGER" // Stop name
                ].join(", ") + ", CONSTRAINT unq UNIQUE (key, stopidfrom, stopidto, stopidvia) ON CONFLICT REPLACE)");
                
                callback && callback();
            });
        } catch(err) {
            console.log("Error creating table in database: " + err);
        }
    }
    return this;
}

DBConnection.prototype.clearSettingsTable = function() {
    if(this.enabled && this.db) {
        try {
            this.db.transaction(function(tx){
                tx.executeSql("DELETE from settings");
            });
        } catch(err) {
            console.log("Error deleting user data: " + err);
        };
    }
}

DBConnection.prototype.saveSetting = function(key, value) {
    if(this.enabled && this.db) {
        this.db.transaction(function(tx) {
            tx.executeSql("INSERT OR REPLACE INTO settings VALUES(?, ?)", [key, value]);
        });
    }
}

DBConnection.prototype.getSetting = function(key) {
    if(this.enabled && this.db) {
        var returnValue = null;
        this.db.transaction(function(tx) {
            var rs = tx.executeSql("SELECT value FROM settings WHERE key=?", [key]);
            if(rs.rows.item(0)) {
                returnValue = rs.rows.item(0).value;
            }
        });
        return returnValue;
    }
    return false;
}

DBConnection.prototype.clearDataJSONTable = function() {
    if(this.enabled && this.db) {
        try {
            this.db.transaction(function(tx){
                tx.executeSql("DELETE from datajson");
            });
        } catch(err) {
            console.log("Error deleting user data: " + err);
        };
    }
}

DBConnection.prototype.saveDataJSON = function(key, value) {
    if(this.enabled && this.db) {
        this.db.transaction(function(tx) {
            tx.executeSql("INSERT OR REPLACE INTO datajson(key, value) VALUES(?, ?)", [key, value]);
        });
    }
}

DBConnection.prototype.getDataJSON = function(key) {
    if(this.enabled && this.db) {
        var returnValue = null;
        this.db.transaction(function(tx) {
            var rs = tx.executeSql("SELECT key,date,value FROM datajson WHERE key=?", [key]);
            var item = rs.rows.item(0);
            if(item) {
                returnValue = {
                    key: item.key,
                    date: item.date,
                    value: item.value
                };
            }
        });
        return returnValue;
    }
    return false;
}

DBConnection.prototype.clearStations = function() {
    if(this.enabled && this.db) {
        try {
            this.db.transaction(function(tx){
                tx.executeSql("DELETE from stops");
            });
        } catch(err) {
            console.log("Error deleting user data: " + err);
        };
    }
}

DBConnection.prototype.clearStationsForId = function(transportId) {
    console.log("Deleting stops for transport option with id: " + transportId);
    if(this.enabled && this.db) {
        try {
            this.db.transaction(function(tx){
                tx.executeSql("DELETE from stops where key=?", [transportId]);
            });
        } catch(err) {
            console.log("Error deleting stops for " + transportId + ": " + err);
        };
    }
}

DBConnection.prototype.saveStation = function(key, data) {
    var id = null;
    if(this.enabled && this.db) {
        data = data || {};
        console.log("saveStation data", "key", key, "data.value", data.value, "data.item", data.item, "data.coorX", data.coorX, "data.coorY", data.coorY);
        if(key && data.value && data.item !== undefined && data.coorX && data.coorY) {
            this.db.transaction(function(tx) {
                /*
                 * CHAPS api does not have a unique station id common for all the station locations.
                 * 
                 * "item" value of each stop is variable and changes as time passes.
                 * "listId" is the same for each stop of a certain "key".
                 * "id" is a generated unique item id.
                 * 
                 * Coordinates are preserved unless a station is moved to a new location due to a reconstruction.
                 * So the question is what's the best approach here. Currently matching by a unique name but not a stop location
                 * even though that could be useful for map view, but sometimes the coordinates are just much too close.
                 */
                 
                // Ok, so let's find all saved stations that could match what we are trying to save
                //var lines = tx.executeSql("SELECT ID as id FROM stops WHERE key=? AND value=? AND coorX=? AND coorY=?", [key, data.value, data.coorX, data.coorY]);
                var lines = tx.executeSql("SELECT ID as id FROM stops WHERE key=? AND value=?", [key, data.value]);
                
                // If there are any matches it means this station is already saved, so we should update it
                var linesAffected = 0;
                if(lines.rows.length > 0) {
                    id = lines.rows.item(0).id;
                    
                    //OLD Approach: linesAffected = tx.executeSql("INSERT OR REPLACE INTO stops VALUES(?, ?, ?, ?, ?, ?, ?)", [id, key, data.item, data.listId, data.value, data.coorX, data.coorY]);
                    
                    // So, updating the entry match with what we got - replacing all but id
                    var replaceReply = tx.executeSql("UPDATE stops set item = ?, listID = ?, value = ?, coorX = ?, coorY = ? WHERE id = ? AND key = ? AND value = ?", [data.item, data.listId, data.value, data.coorX, data.coorY, id, key, data.value]);
                    linesAffected = replaceReply.rowsAffected || 0;
                    console.log("Replacing entry with updated data:", data.value, replaceReply.rowsAffected ? "OK" : "Nothing to replace", ", on id", id);
                    
                    // It is however possible we missed something (this was expecially true when relying on item and listId values)
                    // Since 1.7.9+ this should never happen and the code should never jump into the following sequence. Keeping it in case it does occur though.
                    // So let's see if there are more matches in the databse for this station:
                    if(lines.rows.length > 1) {
                        console.log("Workaround for a CHAPS api issue:\n");
                        console.log("Attention! The dababase has " + lines.rows.length + " stop duplicates: [" + key + "] " + data.value);
                        console.log("(", "id:", id, "item:", data.item, "listId:", data.listId, "value:", data.value, ")");
                        console.log("Deleting duplicates of: " + id);
                        
                        var valueReplacementMismatch = false;
                        for(var i = 0; i < lines.rows.length; i++) {
                            if(id === lines.rows.item(i).id) {
                                if(lines.rows.item(i).value === data.value) {
                                    console.log("Old entry value matches new entry value", data.value);
                                    console.log("Ignoring deletion, retaining old stop data");
                                    valueReplacementMismatch = true;
                                }
                            }
                        }
                        
                        if(!valueReplacementMismatch) {
                            for(var i = 0; i < lines.rows.length; i++) {
                                if(id !== lines.rows.item(i).id) {
                                    console.log("Deleting station with id: " + lines.rows.item(i).id)
                                    tx.executeSql("DELETE from stops where id=? AND key=?", [lines.rows.item(i).id, key]);
                                }
                            }
                        }
                    }
                    
                    if(replaceReply.rowsAffected === 0) {
                        // Insert if update fails due to missing db match - this should not happen!
                        var insertReply = tx.executeSql("INSERT INTO stops VALUES(null, ?, ?, ?, ?, ?, ?)", [key, data.item, data.listId, data.value, data.coorX, data.coorY]);
                        console.log("Inserting new entry because update failed:", data.value, insertReply.insertId);
                        id = insertReply.insertId;
                        linesAffected = insertReply.rowsAffected || 0;
                    }
                }
                else {
                    // Insert if there is no db match present
                    var insertReply = tx.executeSql("INSERT OR REPLACE INTO stops VALUES(null, ?, ?, ?, ?, ?, ?)", [key, data.item, data.listId, data.value, data.coorX, data.coorY]);
                    console.log("Inserting new entry:", data.value, insertReply.insertId);
                    id = insertReply.insertId;
                    linesAffected = insertReply.rowsAffected || 0;
                }

                if(linesAffected !== 1) {
                    console.log("An error occured while inserting or updating stop [" + key + "] " + data.value);
                    console.log("linesAffected in DB", linesAffected);
                }
            });
        }
        else {
            console.log("Error: Station could not be saved to DB: " + data.value);
        }
    }
    return id;
}

DBConnection.prototype.getStationsByName = function(key, value) {
    if(this.enabled && this.db) {
        var startsWithMatches = [];
        var laterMatches = [];
        var searchBaseString = GeneralTranport.baseString(value);

        this.db.transaction(function(tx) {
            var rs = tx.executeSql("SELECT ID as id,key,value,item,listId,coorX,coorY FROM stops WHERE key=? ORDER BY value ASC", [key]);
            for(var i = 0; i < rs.rows.length; i++) {
                if(rs.rows.item(i) !== undefined && rs.rows.item(i).value) {
                    if(startsWithMatches.length <= 10) {
                        var item = rs.rows.item(i);
                        var stopObj = {
                            id: item.id,
                            key: item.key,
                            value: item.value,
                            item: item.item,
                            listId: item.listId,
                            coorX: item.coorX,
                            coorY: item.coorY
                        };
                        
                        var dbStopBaseString = GeneralTranport.baseString(item.value);
                        if(dbStopBaseString.indexOf(searchBaseString) === 0) {
                            startsWithMatches.push(stopObj);
                        }
                        else if(dbStopBaseString.indexOf(searchBaseString) > -1) {
                            laterMatches.push(stopObj);
                        }
                    }
                }
            }
        });

        var returnValue = startsWithMatches.concat(laterMatches);
        return returnValue.slice(0, 10);
    }
    return false;
}

DBConnection.prototype.getStationByValue = function(key, value) {
    if(this.enabled && this.db) {
        var returnValue = null;
        this.db.transaction(function(tx) {
            var rs = tx.executeSql("SELECT ID as id,key,value,item,listId,coorX,coorY FROM stops WHERE key=? AND value=?", [key, value]);
            var item = rs.rows.item(0);
            if(item) {
                returnValue = {
                    id: item.id,
                    key: item.key,
                    value: item.value,
                    item: item.item,
                    listId: item.listId,
                    coorX: item.coorX,
                    coorY: item.coorY
                };
            }
        });
        return returnValue;
    }
    return false;
}

DBConnection.prototype.getAllStations = function(key) {
    if(this.enabled && this.db) {
        var stations = [];
        this.db.transaction(function(tx) {
            var rs = {};
            if(key) {
                rs = tx.executeSql("SELECT ID as id,key,value,item,listId,coorX,coorY FROM stops WHERE key=?", [key]);
            }
            else {
                rs = tx.executeSql("SELECT ID as id,key,value,item,listId,coorX,coorY FROM stops");
            }
            for(var i = 0; i < rs.rows.length; i++) {
                var item = rs.rows.item(i);
                if(item) {
                    stations.push({
                        id: item.id,
                        key: item.key,
                        value: item.value,
                        item: item.item,
                        listId: item.listId,
                        coorX: item.coorX,
                        coorY: item.coorY
                    });
                }
            }
        });
        return stations;
    }
    return false;
}

DBConnection.prototype.dumpAllStations = function(key) {
    console.log("dumping All Stations:\n");
    console.log(JSON.stringify(this.getAllStations(key)));
    console.log("\n\n");
}

DBConnection.prototype.cleanDupliciousStations = function(key) {
    if(this.enabled && this.db) {
        this.db.transaction(function(tx) {
            var rs = {};
            if(key) {
                rs = tx.executeSql("SELECT ID as id,key,value,item,listId,coorX,coorY FROM stops WHERE key=?", [key]);
            }
            else {
                rs = tx.executeSql("SELECT ID as id,key,value,item,listId,coorX,coorY FROM stops");
            }
            
            var names = [];
            var items = [];
            
            for(var i = 0; i < rs.rows.length; i++) {
                var item = rs.rows.item(i);
                if(item) {
                    var nameIndex = names.indexOf(item.value);
                    var deleteStation = false;
                    if(nameIndex >= 0) {
                        console.log("duplicious station by name found", item.value, nameIndex);
                        deleteStation = true;
                    }
                    
                    var itemIndex = items.indexOf(item.item);
                    if(itemIndex >= 0) {
                        console.log("duplicious station by item found", item.item, itemIndex);
                        deleteStation = true;
                    }
                    
                    if(deleteStation) {
                        console.log("Deleting station with id: " + item.id);
                        if(key) {
                            tx.executeSql("DELETE from stops where id=? AND key=?", [item.id, key]);
                        }
                        else {
                            tx.executeSql("DELETE from stops where id=?", [item.id]);
                        }
                    }
                    
                    names.push(item.value);
                    items.push(item.item);
                }
            }
        });
        return true;
    }
    return false;
}

DBConnection.prototype.getNearbyStopsByKey = function(transportId, coor) {
    if(this.enabled && this.db) {
        var lcoorX = coor.x || coor.latitude;
        var lcoorY = coor.y || coor.longitude;
                
        var returnValue = [];
        this.db.transaction(function(tx) {
            var fudge = Math.pow(Math.cos((lcoorX) * Math.PI / 180), 2);
            var rs = tx.executeSql("SELECT ID as id,key,value,item,listId,coorX,coorY FROM stops WHERE key=? AND coorX IS NOT NULL AND coorY IS NOT NULL ORDER BY ((? - coorX) * (? - coorX) + (? - coorY) * (? - coorY) * ?) ASC LIMIT 10", [transportId, lcoorX, lcoorX, lcoorY, lcoorY, fudge]);
            for(var i = 0; i < rs.rows.length; i++) {
                var item = rs.rows.item(i);
                
                if(latLongDistance(lcoorX, lcoorY, item.coorX, item.coorY) > 2) {
                    break;
                }
                
                returnValue.push({
                    id: item.id,
                    key: item.key,
                    value: item.value,
                    item: item.item,
                    listId: item.listId,
                    coorX: item.coorX,
                    coorY: item.coorY
                });
            }
        });
        return returnValue;
    }
    return false;
}

DBConnection.prototype.getNearbyStops = function(coor, limit) {
    if(this.enabled && this.db) {
        var lcoorX = coor.x || coor.latitude;
        var lcoorY = coor.y || coor.longitude;
                
        var returnValue = [];
        this.db.transaction(function(tx) {
            var fudge = Math.pow(Math.cos((lcoorX) * Math.PI / 180), 2);
            var rs = tx.executeSql("SELECT ID as id,key,value,item,listId,coorX,coorY FROM stops WHERE coorX IS NOT NULL AND coorY IS NOT NULL ORDER BY ((? - coorX) * (? - coorX) + (? - coorY) * (? - coorY) * ?) ASC LIMIT ?", [lcoorX, lcoorX, lcoorY, lcoorY, fudge, limit]);
            for(var i = 0; i < rs.rows.length; i++) {
                var item = rs.rows.item(i);
                
                if(latLongDistance(lcoorX, lcoorY, item.coorX, item.coorY) > 2) {
                    break;
                }
                
                returnValue.push({
                    id: item.id,
                    key: item.key,
                    value: item.value,
                    item: item.item,
                    listId: item.listId,
                    coorX: item.coorX,
                    coorY: item.coorY
                });
            }
        });
        return returnValue;
    }
    return false;
}

DBConnection.prototype.getAllUsedTypesFromSavedStations = function() {
    if(this.enabled && this.db) {
        var returnValue = [];
        this.db.transaction(function(tx) {
            var rs = tx.executeSql("SELECT DISTINCT key FROM stops");
            var item = rs.rows;
            for(var i = 0; i < rs.rows.length; i++) {
                if(item.item(i).key) {
                    returnValue.push(item.item(i).key);
                }
            }
        });
        return returnValue;
    }
    return false;
}

// Append new search to history
DBConnection.prototype.appendSearchToHistory = function(search) {
    var success = false;
    if(this.enabled && this.db) {
        if(search) {            
            var self = this;
            this.db.transaction(function(tx) {
                var countOfLines = tx.executeSql("SELECT Count(*) as count FROM recent").rows.item(0).count;
                var linesAffected = 0;
                var limit = 120; /* Limit recent search results */

                if(!search.from || !search.to) {
                    console.log("Can't save history entry without initial and final station.");
                    success = false;
                    return false;
                }
                if(!search.via) {
                    search.via = -1;
                }
                
                var matchedLines = tx.executeSql("SELECT Count(*) as count FROM recent WHERE key=? AND stopidfrom=? AND stopidto=? AND stopidvia=?", [search.id, search.from, search.to, search.via]);
                if(matchedLines.rows.item(0).count <= 0) {
                    if(countOfLines < limit) {
                        linesAffected = tx.executeSql("INSERT OR REPLACE INTO recent(key, stopidfrom, stopidto, stopidvia) VALUES(?, ?, ?, ?)", [search.id, search.from, search.to, search.via]);
                        if(linesAffected.rowsAffected !== 1) {
                            console.log("An error occured while creating or updating search history.");
                        }
                    }
                    else {
                        linesAffected = tx.executeSql("DELETE FROM recent WHERE ID in (SELECT ID FROM recent AS selector INNER JOIN (SELECT ID as sid, date FROM recent ORDER BY date LIMIT 1) AS limited ON selector.ID = limited.sid)");
                        if(linesAffected.rowsAffected !== 1) {
                            console.log("An error occured while deleting an entry in the search history.");
                        }

                        success = self.appendSearchToHistory(search);
                        return false;
                    }
                }
                else {
                    var dbFix = tx.executeSql("SELECT * FROM recent");
                    var fixed = false;
                    for(var i = 0; i < dbFix.rows.length; i++) {
                        if(!dbFix.rows.item(i).stopidfrom || !dbFix.rows.item(i).stopidto) {
                            if(dbFix.rows.item(i).ID) {
                                self.deleteSearchHistory(dbFix.rows.item(i).ID);
                                fixed = true;
                            }
                            else {
                                self.deleteAllSearchHistory();
                                fixed = true;
                                console.log("Search histroy deleted due to a DB error");
                            }
                        }
                    }
                    if(fixed) {
                        return self.appendSearchToHistory(search);
                    }

                    linesAffected = tx.executeSql("INSERT OR REPLACE INTO recent(ID, key, stopidfrom, stopidto, stopidvia) VALUES(?, ?, ?, ?, ?)", [matchedLines.rows.item(0).id, search.id, search.from, search.to, search.via]);
                }

                if(linesAffected.rowsAffected !== 1) {
                    console.log("An error occured while creating or updating search history.");
                    success = false;
                }
            });
        }
    }
    return success;
}

// Delete all search history by ID
DBConnection.prototype.deleteAllSearchHistory = function() {
    if(this.enabled && this.db) {
        this.db.transaction(function(tx) {
            var linesAffected = tx.executeSql("DELETE FROM recent");
            if(linesAffected.rowsAffected < 1) {
                console.log("Nothing was deleted in the search history.");
            }
        });
    }
}

// Delete search history by ID
DBConnection.prototype.deleteSearchHistory = function(id) {
    if(this.enabled && this.db) {
        this.db.transaction(function(tx) {
            var linesAffected = tx.executeSql("DELETE FROM recent WHERE ID=?", [id]);
            if(linesAffected.rowsAffected !== 1) {
                console.log("Nothing was deleted in the search history.");
            }
        });
    }
}

// Get a list of recent search history
// Returns ID, key, date, typename and names: stopfrom, stopto and stopvia + coordinates like stopfromx and stopfromy
DBConnection.prototype.getSearchHistory = function() {
    var searches = [];
    if(this.enabled && this.db) {
        this.db.transaction(function(tx) {
            var rs = tx.executeSql("SELECT date, recent.key as key, recent.ID as ID, stopidfrom, stopsfrom.value as stopnamefrom, stopidto, stopsto.value as stopnameto, stopidvia, stopsvia.value as stopnamevia FROM recent INNER JOIN stops stopsfrom ON (stopidfrom = stopsfrom.ID) INNER JOIN stops stopsto ON (stopidto = stopsto.ID) LEFT JOIN stops stopsvia ON (stopidvia = stopsvia.ID) ORDER BY date DESC");
            for(var i = 0; i < rs.rows.length; i++) {
                searches.push(rs.rows.item(i));
            }
        });
    }
    return searches;
}

// Get a list of recent search history for a specified transport type
// Returns ID, key, date, typename and names: stopfrom, stopto and stopvia + coordinates like stopfromx and stopfromy
DBConnection.prototype.getSearchHistoryById = function(key) {
    var searches = [];
    if(this.enabled && this.db) {
        this.db.transaction(function(tx) {
            var rs = tx.executeSql("SELECT date, recent.key as key, recent.ID as ID, stopidfrom, stopsfrom.value as stopnamefrom, stopidto, stopsto.value as stopnameto, stopidvia, stopsvia.value as stopnamevia FROM recent INNER JOIN stops stopsfrom ON (stopidfrom = stopsfrom.ID) INNER JOIN stops stopsto ON (stopidto = stopsto.ID) LEFT JOIN stops stopsvia ON (stopidvia = stopsvia.ID) WHERE recent.key=? ORDER BY date DESC", [key])
            for(var i = 0; i < rs.rows.length; i++) {
                searches.push(rs.rows.item(i));
            }
        });
    }
    return searches;
}
