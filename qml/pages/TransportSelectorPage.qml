import QtQuick 2.9
import Lomiri.Components 1.3
import QtQuick.LocalStorage 2.0
import QtQuick.Layouts 1.2

import "../components"

import "../transport-api.js" as Transport

Page {
    id: transportSelectorPage
    header: PageHeader {
        id: pageHeader
        title: i18n.tr("Select a transport option")

        StyleHints {
            foregroundColor: pageLayout.colorPalete["headerText"]
            backgroundColor: pageLayout.colorPalete["headerBG"]
        }

        trailingActionBar {
            actions: [
                Action {
                    iconName: "reload"
                    text: i18n.tr("Refresh transport options")
                    onTriggered: {
                        serverUpdate();
                    }
                },
                Action {
                    iconName: transportSelectorPage.editColors ? "cancel" : "edit"
                    text: i18n.tr("Edit colors")
                    enabled: usedListModel.count > 0
                    onTriggered: {
                        transportSelectorPage.editColors = !transportSelectorPage.editColors
                    }
                }
           ]
        }
    }

    clip: true
    
    property var editColors: false
    property var filterValue: ""
    property var allTransportOptionsCount: 0
    property var activeTimers: []
    
    onEditColorsChanged: {
        restListView.visible = false;
    }

    onFilterValueChanged: update()

    function serverUpdate() {
        progressLine.state = "running";
        Transport.transportOptions.fetchTrasports({
            forceServer: true,
            callback: () => {
                progressLine.state = "idle";
            }
        });
    }

    function update(options = {}) {
        const { slow = false, slowGoupJump = 1, slowGroups = 2, slowSpeed = 100 } = options;

        /* Get rid of potential running timers from previous iterations (e.g. if user is too quick clicking). */
        activeTimers.forEach(activeTimer => activeTimer && typeof activeTimer.destroy === "function" && activeTimer.destroy());
        activeTimers = [];

        progressLine.state = "running";

        const minifiedFilterValue = Transport.accentFold(filterValue.trim().toLowerCase());
        const compareToFilter = item => Object.values(item).filter(value => typeof value === "string").find(value =>
            !minifiedFilterValue || Transport.accentFold(value.trim().toLowerCase()).includes(minifiedFilterValue)
        );

        Transport.transportOptions.setTransportUpdateCallback((options, state) => {
            progressLine.state = "idle";
            
            if(!state || state === "SUCCESS") {
                if(options) {                    
                    const langCode = Transport.langCode(true);

                    allTransportOptionsCount = (options.transports || []).length;
                    const { restList, usedList } = (options.transports || []).reduce((accumulator, transport, index, allTransportOptions) => {
                        const trTypesObj = transport.getTrTypes(null, langCode);
                        const trTypes = trTypesObj.map(({ name }) => name).join(", ");

                        const item = {
                            id: transport.getId(),
                            name: transport.getName(langCode),
                            nameExt: transport.getNameExt(langCode),
                            description: transport.getDescription(langCode),
                            title: transport.getTitle(langCode),
                            city: transport.getCity(langCode),
                            homeState: transport.getHomeState(),
                            ttValidFrom: transport.getTimetableInfo().ttValidFrom,
                            ttValidTo: transport.getTimetableInfo().ttValidTo,
                            isUsed: transport.isUsed(),
                            trTypes
                        };

                        return {
                            restList: accumulator.restList.concat(compareToFilter(item) ? item : []),
                            /* Only show separate list when there are more than 10 options available */
                            usedList: accumulator.usedList.concat((item.isUsed && allTransportOptions.length > 10) ? item : [])
                        };
                    }, { restList: [], usedList: [] });

                    restListModel.clear();

                    if(slow) {
                        restList.reduce((accumulator, item, index) => {
                            /* Slow load, jump by groups */
                            const arrayGroupIndex = Math.floor(index / slowGoupJump);
                            const finalArrayIndex = arrayGroupIndex >= slowGroups ? slowGroups : arrayGroupIndex;
                            accumulator[finalArrayIndex] = accumulator[finalArrayIndex] || [];
                            accumulator[finalArrayIndex].push(item);
                            return accumulator;
                        }, []).forEach((item, index) => {
                            const timer = Qt.createQmlObject("import QtQuick 2.9; Timer {}", transportSelectorPage);
                            timer.interval = slowSpeed * index;
                            timer.repeat = false;
                            timer.triggered.connect(() => {
                                if(typeof timer.destroy === "function") {
                                    restListModel.append( item );
                                    timer.destroy();
                                }
                            });
                            timer.start();
                            activeTimers.push(timer);
                        });
                    }
                    else {
                        restListModel.append( restList );
                    }

                    usedListModel.clear();
                    usedListModel.append( usedList );
                }
            }
            else {
                if(state !== "ABORT") {
                    errorMessage.value = i18n.tr("Failed to update transport types info");
                }
            }
        });

        Transport.transportOptions.fetchTrasports({});
    }

    ProgressLine {
        id: progressLine
        anchors {
            top: pageHeader.bottom
        }
        z: 10
    }
    
    ErrorMessage {
        id: errorMessage
        anchors.top: pageHeader.bottom
    }
    
    TransportDelegate {
        id: transportDelegate
    }

    Flickable {
        id: transportSelectorFlickable
        anchors {
            top: errorMessage.bottom
            right: parent.right
            bottom: parent.bottom
            left: parent.left
        }
        contentWidth: parent.width
        contentHeight: transportSelectorColumn.height
        flickableDirection: Flickable.VerticalFlick

        Column {
            id: transportSelectorColumn
            anchors {
                left: parent.left
                right: parent.right
            }
            height: childrenRect.height

            ListItem {
                visible: usedListModel.count > 0 && usedListModel.count !== restListModel.count

                Rectangle {
                    anchors {
                        margins: units.gu(2)
                        left: parent.left
                        right: parent.right
                    }
                    height: parent.height
                    color: "transparent"

                    Label {
                        text: i18n.tr("Used transport types") + ` (${ usedListModel.count })`
                        width: parent.width
                        font.italic: true
                        wrapMode: Text.WordWrap
                        anchors.verticalCenter: parent.verticalCenter
                    }
                }
            }

            ListView {
                id: usedListView
                width: parent.width
                height: contentHeight
                interactive: false
                model: ListModel {
                    id: usedListModel
                }
                delegate: transportDelegate
            }

            ListItem {
                visible: !transportSelectorPage.editColors
                Rectangle {
                    anchors {
                        margins: units.gu(2)
                        left: parent.left
                        right: parent.right
                    }
                    height: parent.height
                    color: "transparent"

                    Label {
                        text: {
                            const allTransportTypes = i18n.tr("All transport types");
                            const restCount = restListModel.count;
                            if(restCount !== allTransportOptionsCount) {
                                return allTransportTypes + " (" + restCount + "/" + allTransportOptionsCount + ")";
                            }
                            return allTransportTypes + " (" + restCount + ")";
                        }
                        width: parent.width
                        font.italic: true
                        wrapMode: Text.WordWrap
                        anchors.verticalCenter: parent.verticalCenter
                    }

                    ActivityIndicator {
                        anchors.centerIn: parent
                        running: progressLine.state === "running"
                        z: 1
                    }

                    Button {
                        width: units.gu(4)
                        anchors {
                            right: parent.right
                            verticalCenter: parent.verticalCenter
                        }
                        visible: usedListModel.count > 0
                        color: "transparent"

                        onClicked: {
                            restListView.visible = !restListView.visible;
                            if(!restListView.visible) {
                                transportSelectorPage.filterValue = "";
                            }
                            else {
                                transportSelectorPage.update({ slow: true });
                            }
                        }

                        Icon {
                            anchors {
                                fill: parent
                                margins: units.gu(0.75)
                            }
                            name: restListView.visible ? "close" : "add"
                            color: pageLayout.colorPalete.baseText
                        }
                    }
                }
            }

            ListItem {
                visible: !transportSelectorPage.editColors

                Rectangle {
                    anchors.fill: parent
                    color: pageLayout.colorPalete["secondaryBG"]

                    TextField {
                        id: filterField
                        anchors {
                            fill: parent
                            margins: units.gu(1)
                            centerIn: parent
                        }
                        hasClearButton: true
                        placeholderText: i18n.tr("Filter transport options")

                        text: transportSelectorPage.filterValue
                        onDisplayTextChanged: filterTimer.restart()
                    }

                    Timer {
                        id: filterTimer
                        interval: 500
                        running: false
                        repeat: false
                        onTriggered: transportSelectorPage.filterValue = filterField.displayText
                    }
                }
            }

            ListView {
                id: restListView
                width: parent.width
                height: visible ? contentHeight : 0
                interactive: false
                model: ListModel {
                    id: restListModel
                }
                delegate: transportDelegate

                property var usedListModelCount: 0

                onUsedListModelCountChanged: visible = usedListModelCount === 0 || transportSelectorPage.filterValue
                Connections {
                    target: transportSelectorPage
                    onFilterValueChanged: restListView.visible = restListView.usedListModelCount === 0 || transportSelectorPage.filterValue
                }

                Component.onCompleted: {
                    Transport.transportOptions.dbConnection.onLoad(function() {
                        transportSelectorPage.update();
                        searchPage.ignoreGps = false;
                        usedListModelCount = usedListModel.count;
                    });
                }
            }
        }
    }

    Scrollbar {
        flickableItem: transportSelectorFlickable
        align: Qt.AlignTrailing
    }
}
