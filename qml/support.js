"use strict";

var Support = function(data) {
    data = data || {};
    this.eventListener = data.eventListener || null;
}

Support.prototype.connectInput = function(data) {
    this.input = {
        from: data.from || null,
        to: data.to || null,
        via: data.via || null,
        dateTimeSelector: data.dateTimeSelector || null,
        arrivalDeparturePicker: data.arrivalDeparturePicker || null,
        advancedSearchSwitch: data.advancedSearchSwitch || null
    };
    return this.input;
}

/* Search for connections in between two stations/locations */
Support.prototype.searchConnections = function(data) {
    var selectedTransport = data.selectedTransport || null;
    
    if(selectedTransport) {
        this.input.from.abort();
        this.input.to.abort();
        this.input.via.abort();
        selectedTransport.abortAll();
        
        var dateTime = null;
        if(this.input.dateTimeSelector.customDate) {
            dateTime = this.input.dateTimeSelector.dateTime;
        }
        
        var fromVal = this.input.from.stopData;
        var toVal = this.input.to.stopData;
        var viaVal = this.input.advancedSearchSwitch.checked ? this.input.via.stopData : null;
        
        if(fromVal && toVal) {
            var connection = selectedTransport.createConnection({
                from: fromVal,
                to: toVal,
                via: viaVal,
                departure: this.input.arrivalDeparturePicker.departure || false, // False = search for arrival at set/current time
                time: dateTime || null // "date time"
            });
            
            this.eventListener.update({
                name: "fetching-connections",
                response: true
            });
            
            var self = this;
            connection.search({}, function(object, state) {
                self.eventListener.update({
                    name: "fetching-connections",
                    response: false
                });
                
                if(state) {
                    if(state === "ABORT") {
                    }
                    else if(state === "FAIL") {
                        self.eventListener.update({
                            name: "fetching-connections-error",
                            response: "fail"
                        });
                    }
                    else if(state === "SUCCESS") {
                        if(object) {
                            self.eventListener.update({
                                name: "fetching-connections-success",
                                response: {
                                    selectedTransport: selectedTransport,
                                    connection: connection
                                }
                            });
                        }
                    }
                }
            });
        }
    }
}

/* Search for departures/arrivals at a certain stations */
Support.prototype.searchDepartures = function(data) {
    var selectedTransport = data.selectedTransport || null;
    var stop = data.stop || null;
    
    if(selectedTransport) {
        selectedTransport.abortAll();
        if(stop) {
            this.eventListener.update({
                name: "fetching-connections",
                response: true
            });
            
            var self = this;
            selectedTransport.departures({
                stop: stop,
                isDep: data.departure || false,
                time: data.dateTime || null, // "date time"
                limit: data.limit || null // "date time"
            }, function(departures, state) {
                self.eventListener.update({
                    name: "fetching-connections",
                    response: false
                });
                
                if(state) {
                    if(state === "FAIL") {
                        self.eventListener.update({
                            name: "fetching-departures-error",
                            response: "fail"
                        });
                    }
                    else if(state === "SUCCESS") {
                        self.eventListener.update({
                            name: "fetching-departures-success",
                            response: departures
                        });
                    }
                }
            });
        }
    }
}

/* Select Defined Stations */
Support.prototype.selectStations = function(data) {
    if(data.selectedTransport) {
        if(data.stopidfrom >= 0 && data.stopnamefrom) {
            setStopData({
                Stop: data.TransportStop,
                dbConnection: data.dbConnection,
                typeid: data.selectedTransport.key,
                stopSearch: this.input.from,
                stopidfrom: data.stopidfrom,
                stopnamefrom: data.stopnamefrom
            });
        }
        if(data.stopidto >= 0 && data.stopnameto) {
            setStopData({
                Stop: data.TransportStop,
                dbConnection: data.dbConnection,
                typeid: data.selectedTransport.key,
                stopSearch: this.input.to,
                stopidfrom: data.stopidto,
                stopnamefrom: data.stopnameto
            });
        }
        if(data.stopidvia >= 0 && data.stopnamevia) {
            setStopData({
                Stop: data.TransportStop,
                dbConnection: data.dbConnection,
                typeid: data.selectedTransport.key,
                stopSearch: this.input.via,
                stopidfrom: data.stopidvia,
                stopnamefrom: data.stopnamevia
            });
            this.input.advancedSearchSwitch.checked = true;
        }
        else {
            this.input.via.empty();
            this.input.advancedSearchSwitch.checked = false;
        }
    }
}
