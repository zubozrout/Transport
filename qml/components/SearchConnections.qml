import QtQuick 2.9
import QtQuick.Layouts 1.3
import Lomiri.Components 1.3

import "../transport-api.js" as Transport
import "../generalfunctions.js" as GeneralFunctions

Item {
    id: searchConnectionsContainer
    
    function search() {
        searchPage.ignoreGps = (progressLine.state === "running");
        
        Transport.support.searchConnections({
            selectedTransport: Transport.transportOptions.getSelectedTransport()
        });

        /* Delete departures last stop search, makind this information/conection a priority */
        Transport.transportOptions.dbConnection.saveSetting("departures-last-stop", "");
    }

    function lastSearchPopulate() {
        var modelData = Transport.transportOptions.dbConnection.getSearchHistory();
        if(modelData.length > 0) {
            Transport.transportOptions.selectTransportById(modelData[0].key);
            populateSearch(modelData[0]);
        }
    }
    
    function populateSearch(historyData) {
        Transport.support.selectStations({
            TransportStop: Transport.Stop,
            dbConnection: Transport.transportOptions.dbConnection,
            selectedTransport: Transport.transportOptions.getSelectedTransport(),
            stopidfrom: historyData.stopidfrom || null,
            stopnamefrom: historyData.stopnamefrom || null,
            stopidto: historyData.stopidto || null,
            stopnameto: historyData.stopnameto || null,
            stopidvia: historyData.stopidvia || null,
            stopnamevia: historyData.stopnamevia || null
        });
    }
    
    // Match a batch of stations with the saved history (based on name instead of ID as we don't necesarilly have to have a DB ID at this point.
    function findRouteInHistoryByStations(data) {
        data = data || {};
        var stops = data.stops || [];
        var justStop = data.justStop || false;
        var callback = data.callback || null;
        var searchHistory = Transport.transportOptions.dbConnection.getSearchHistory();
        var stationFound = false;
        
        if(!justStop) {
            // Search for a searched route with the closest from or to station
            var stopsFoundInSearchHistory = [];
            for(var i = 0; i < stops.length; i++) {
                var stop = stops[i];
                
                for(var j = 0; j < searchHistory.length; j++) {
                    var historyItem = searchHistory[j];
                    
                    var historyItemAlreadyRegistered = false;
                    for(var k = 0; k < stopsFoundInSearchHistory.length; k++) {
                        if(stopsFoundInSearchHistory[k].historyIndex === j) {
                            historyItemAlreadyRegistered = true;
                            break;
                        }
                    }
                    
                    if(!historyItemAlreadyRegistered && stop.getTransportId() === historyItem.key) {
                        if(stop.getName() === historyItem.stopnamefrom) {
                            stopsFoundInSearchHistory.push({
                                type: "from",
                                stop: stop,
                                closestIndex: i,
                                historyIndex: j,
                                searchHistory: historyItem
                            });
                        }
                        else if(stop.getName() === historyItem.stopnameto) {
                            var searchHistoryCopy = JSON.parse(JSON.stringify(historyItem));
                            if(historyItem.stopidto >= 0 && historyItem.stopnameto) {
                                searchHistoryCopy.stopidfrom = historyItem.stopidto;
                                searchHistoryCopy.stopnamefrom = historyItem.stopnameto;
                            }
                            if(historyItem.stopidfrom >= 0 && historyItem.stopnamefrom) {
                                searchHistoryCopy.stopidto = historyItem.stopidfrom;
                                searchHistoryCopy.stopnameto = historyItem.stopnamefrom;
                            }
                            
                            stopsFoundInSearchHistory.push({
                                type: "to",
                                stop: stop,
                                closestIndex: i,
                                historyIndex: j,
                                searchHistory: searchHistoryCopy
                            });
                        }
                    }
                }
            }
            
            if(stopsFoundInSearchHistory.length > 0) {
                stopsFoundInSearchHistory.sort(function(a, b) {
                    var indexDestination = (a.type === "from" ? -1 : 1) - (b.type === "from" ? -1 : 1);
                    var indexPosition = a.closestIndex - b.closestIndex;
                    var indexHistory = a.historyIndex - b.historyIndex;
                    return indexPosition || indexHistory || indexDestination;
                    // return Math.min(indexPosition, 0.5 * indexHistory);
                });
                
                Transport.transportOptions.selectTransportById(stopsFoundInSearchHistory[0].searchHistory.key);
                populateSearch(stopsFoundInSearchHistory[0].searchHistory);
                stationFound = true;
            }
        }
        
        // Place at least the closest stop to the "From" field if no route match was found
        if(!stationFound) {
            for(var i = 0; i < stops.length; i++) {
                var stop = stops[i];
                var newSelectedTransport = Transport.transportOptions.selectTransportById(stop.getTransportId());
                try {
                    GeneralFunctions.setStopData({
                        Stop: Transport.Stop,
                        dbConnection: newSelectedTransport.dbConnection,
                        stopSearch: from,
                        stopidfrom: stop.getId(),
                        stopnamefrom: stop.getName(),
                        typeid: stop.getTransportId()
                    });
                }
                catch (error) {
                    console.error(error);
                }
                
                if(!justStop) {
                    var toStationFound = false;
                    for(var j = 0; j < searchHistory.length; j++) {
                        var historyItem = searchHistory[j];
                        if(historyItem.key === stop.getTransportId()) {
                            if(historyItem.stopnameto !== stop.getName()) {
                                GeneralFunctions.setStopData({
                                    Stop: Transport.Stop,
                                    dbConnection: newSelectedTransport.dbConnection,
                                    stopSearch: to,
                                    stopidfrom: historyItem.stopidto,
                                    stopnamefrom: historyItem.stopnameto,
                                    typeid: historyItem.key
                                });
                                toStationFound = true;
                                break;
                            }
                            else if(historyItem.stopnamefrom !== stop.getName()) {
                                GeneralFunctions.setStopData({
                                    Stop: Transport.Stop,
                                    dbConnection: newSelectedTransport.dbConnection,
                                    stopSearch: to,
                                    stopidfrom: historyItem.stopidfrom,
                                    stopnamefrom: historyItem.stopnamefrom,
                                    typeid: historyItem.key
                                });
                                toStationFound = true;
                                break;
                            }
                        }
                    }
                    if(!toStationFound) {
                        to.empty();
                    }
                }
                
                if(to.value === from.value) {
                    to.empty();
                }
                
                via.empty();
                stationFound = true;
                break;
            }
        }
        
        if(callback) {
            callback(stationFound);
        }
        
        return stationFound;
    }
    
    function findClosestRouteInHistory(data, callback) {
        data = data || {};
                
        var coords = data.coords || positionSource.position.coordinate;
        if(!coords.isValid) {
            return false;
        }
        
        var geoLocation = Number(Transport.transportOptions.getDBSetting("geolocation-source") || 0);
        
        console.log("Searching closest stations locally");
        data.stops = data.stops || Transport.transportOptions.searchSavedStationsByLocation(coords);
        findRouteInHistoryByStations(data);
        if(callback) {
            callback(geoLocation !== 0);
        }
        
        if(geoLocation === 0) {
            Transport.eventListener.update({
                name: "geo-search",
                response: true
            });
            
            console.log("Searching closest stations online - firstly narrowed by pre-saved stations to select the city, if possible");
            Transport.transportOptions.getNearbyStopsOnline(coords, null, function(onlineStops, obj) {
                if(!searchPage.ignoreGps) {
                    data.stops = onlineStops || data.stops || Transport.transportOptions.searchSavedStationsByLocation(coords);
                    findRouteInHistoryByStations(data);
                    searchPage.ignoreGps = false;
                }
                if(callback) {
                    callback(true);
                }
                
                Transport.eventListener.update({
                    name: "geo-search",
                    response: false
                });
            });
        }
    }
    
    function init() {
        var notFirstRun = Number(Transport.transportOptions.getDBSetting("not-first-run") || 0);
        if(notFirstRun === 1) {
            var geoLocation = Number(Transport.transportOptions.getDBSetting("geolocation-on-start") || 0);
            if(geoLocation === 0) {
                positionSource.append(function(source) {
                    source.keepActive = false;
                    if(source.isValid && !searchPage.ignoreGps) {
                        searchConnectionsContainer.findClosestRouteInHistory({
                            coords: source.position.coordinate
                        }, function(complete) {
                            // Nothing
                        });
                    }
                    else {
                        errorMessage.value = i18n.tr("Location search disabled or not functional");
                    }
                });
            }
        }
        else {
            pageLayout.addPageToNextColumn(searchPage, welcomePage);
        }
    }
    
    Component.onCompleted: {
        /* Init */
        Transport.support.connectInput({
            from: from,
            to: to,
            via: via,
            dateTimeSelector: dateTimeSelector,
            arrivalDeparturePicker: arrivalDeparturePicker,
            advancedSearchSwitch: advancedSearchSwitch
        });
        
        positionSource.keepActive = true;
        lastSearchPopulate();
        
        Transport.eventListener.listen({
            name: "transport-change",
            callback: function(selectedTransport) {
                console.log("transport-change", selectedTransport ? selectedTransport.getId() : "undefined transport?");
                pageLayout.setHeaderColor(selectedTransport ? Transport.transportOptions.getDBSetting(selectedTransport.getId()) : null);
                searchPage.displaySearchResultsIcons(selectedTransport.getAllConnections().length > 0);
                
                var searchHistory = Transport.transportOptions.dbConnection.getSearchHistory();
                for(var i = 0; i < searchHistory.length; i++) {
                    if(searchHistory[i].key === selectedTransport.getId()) {
                        populateSearch(searchHistory[i]);
                        return;
                    }
                }
                
                from.empty();
                to.empty();
                via.empty();
            }
        });
        
        Transport.eventListener.listen({
            name: "fetching-connections-success",
            callback: function(data) {
                var selectedTransport = data.selectedTransport || null;
                var connection = data.connection || null;
                if(selectedTransport && connection) {
                    connectionsPage.transport = selectedTransport;
                    connectionsPage.connections = connection;
                    connectionsPage.renderAllConnections(connection);
                    connectionsPage.scrollTo(0);
                    pageLayout.addPageToNextColumn(searchPage, connectionsPage);
                    
                    connectionsPage.enableHeaderButtons(selectedTransport.getAllConnections());
                    searchPage.displaySearchResultsIcons(true);
                    Transport.transportOptions.refreshLocal();
                }
            }
        });
    }
    
    Timer {
        id: delayedInitTimer
        interval: 1000
        repeat: false
        running: true
        triggeredOnStart: false

        onTriggered: {
            init();
        }
    }
    
    Flickable {
        id: searchConnectionsFlickable
        anchors.fill: parent
        contentWidth: width
        contentHeight: searchColumn.height + 2 * searchColumn.anchors.margins
        flickableDirection: Flickable.VerticalFlick
        interactive: contentHeight > height
        clip: true
        
        Column {
            id: searchColumn
            anchors {
                left: parent.left
                right: parent.right
                margins: units.gu(1)
            }
            
            spacing: units.gu(2)

            StopSearch {
                id: from
                z: 10

                property var searchFunction: searchConnectionsContainer.search
                
                Component.onCompleted: {
                    setFocusCallback(function(focus) {
                        if(focus) {
                            searchPage.ignoreGps = (progressLine.state === "running");
                        }
                    });
                }
            }
            
            Button {
                id: switchStationsButton
                anchors.horizontalCenter: parent.horizontalCenter
                width: units.gu(3)
                height: width
                color: "transparent"

                onClicked: {
                    var fromData = from.getData();
                    var toData = to.getData();
                    from.setData(toData);
                    to.setData(fromData);
                    
                    searchPage.ignoreGps = (progressLine.state === "running");
                }

                Icon {
                    anchors.fill: parent
                    name: "swap"
                }
            }

            StopSearch {
                id: to
                z: 9

                property var searchFunction: searchConnectionsContainer.search
                
                Component.onCompleted: {
                    setFocusCallback(function(focus) {
                        if(focus) {
                            searchPage.ignoreGps = (progressLine.state === "running");
                        }
                    });
                }
            }

            RowLayout {
                width: parent.width
                spacing: units.gu(2)
                height: childrenRect.height
                Layout.fillWidth: true

                Label {
                    text: i18n.tr("Via")
                    wrapMode: Text.WordWrap
                    Layout.fillWidth: true
                }

                Switch {
                    id: advancedSearchSwitch
                    checked: false
                    Layout.fillWidth: false
                }
            }

            StopSearch {
                id: via
                z: 8
                visible: advancedSearchSwitch.checked

                property var searchFunction: searchConnectionsContainer.search
                
                Component.onCompleted: {
                    setFocusCallback(function(focus) {
                        if(focus) {
                            searchPage.ignoreGps = (progressLine.state === "running");
                        }
                    });
                }
            }

            DateTimeSelector {
                id: dateTimeSelector
            }
            
            RowPicker {
                id: arrivalDeparturePicker

                property bool departure: true
                property bool followTransportColorScheme: true
                property var render: function(model) {
                    clear();
                    initialize([i18n.tr("Departure"), i18n.tr("Arrival")], 0, function(itemIndex) {
                        arrivalDeparturePicker.departure = itemIndex === 0;
                    });
                }

                Component.onCompleted: {
                    arrivalDeparturePicker.update(function(model) { arrivalDeparturePicker.render(model) });
                }
            }

            Rectangle {
                width: parent.width
                height: 1
                color: pageLayout.colorPalete["secondaryBG"]
            }

            RectangleButton {
                id: rectangleButton
                anchors.horizontalCenter: parent.horizontalCenter
                text: itemActivity.running ? i18n.tr("Abort search") : i18n.tr("Search")
                enabled: transportOptionSelected && (from.value || to.value) && from.value !== to.value
                color: active ? pageLayout.colorPalete["headerBG"] : "#ddd"
                z: 1
                
                property bool transportOptionSelected: false

                ActivityIndicator {
                    id: itemActivity
                    anchors {
                        fill: parent
                        centerIn: parent
                        margins: parent.height/6
                    }
                    running: false
                    
                    Component.onCompleted: {
                        Transport.eventListener.listen({
                            name: "fetching-connections",
                            callback: function(value) {
                                if(value) {
                                    itemActivity.running = true;
                                }
                                else {
                                    itemActivity.running = false;
                                }
                            }
                        });
                    }
                }

                Component.onCompleted: {
                    setCallback(function() {
                        if(!itemActivity.running) {
                            searchConnectionsContainer.search();
                        }
                        else {
                            var selectedTransport = Transport.transportOptions.getSelectedTransport();
                            if(selectedTransport) {
                                selectedTransport.abortAll();
                            }
                        }
                    });
                    
                    Transport.eventListener.listen({
                        name: "transport-change",
                        callback: function(selectedTransport) {
                            transportOptionSelected = selectedTransport ? true : false;
                        }
                    });
                }
            }
        }
    }
    
    Scrollbar {
        id: scrollBar
        flickableItem: searchConnectionsFlickable
        align: Qt.AlignTrailing
    }
    
    Keys.onUpPressed: scrollBar.decrease()
    Keys.onDownPressed: scrollBar.increase()
}
