import QtQuick 2.9
import QtQuick.Layouts 1.3
import Lomiri.Components 1.3

Item {
    id: rowPicker
    anchors {
        left: parent.left
        right: parent.right
    }
    height: childrenRect.height
    
    property bool followTransportColorScheme: false
    property bool narrow: false
    property var leftRightMargins: 0
    property var initialCallback: null
    property var callbacks: [{}]

    function setTite(title, setNarrow) {
        titleText.text = title || "";
        narrow = typeof setNarrow !== typeof undefined && setNarrow;
    }
    
    function setFootNote(text) {
        footNoteText.text = text || "";
    }
    
    function setLeftRightMargins(margins) {
        leftRightMargins = margins || 0;
    }
    
    function initialize(options, selectedIndex, callback) {
        var index = Number(selectedIndex) || 0;

        for(var i = 0; i < options.length; i++) {
            append({
                textContent: options[i],
                buttonActive: Number(i) === Number(index) ? true : false
            });

            if(typeof callback !== typeof undefined) {
                setCallbackFor(i, function(itemIndex) {
                    callback(itemIndex);
                });
            }
        }
        
        if(typeof callback !== typeof undefined) {
            initialCallback = callback;
        }
    }

    function append(itemData) {
        itemData.buttonActive = itemData.buttonActive || false;
        rowListModel.append(itemData);
        
        if(!itemData.callback && initialCallback) {
            setCallbackFor(rowListModel.count - 1, function(itemIndex) {
                initialCallback(itemIndex);
            });
        }
    }

    function count() {
        return rowListModel.count;
    }

    function clear() {
        rowListModel.clear();
    }

    function setCallbackFor(n, callback) {
        callbacks[n] = callback;
    }

    function hasCallback(n) {
        if(typeof callbacks[n] === typeof function() {}) {
            return callbacks[n];
        }
        return false;
    }

    function selectItem(n) {
        for(var i = 0; i < rowListModel.count; i++) {
            var item = rowListModel.get(i);
            rowListModel.setProperty(i, "buttonActive", i === n ? true : false);
        }
    }

    function indexData(n) {
        if(n > 0 && n < rowListModel.count) {
            return rowListModel.get(n);
        }
    }

    function hasValue(value) {
        for(var i = 0; i < rowListModel.count; i++) {
            if(rowListModel.get(i).textContent === value) {
                return true;
            }
        }
        return false;
    }

    function update(procedure) {
        if(procedure) {
            procedure(rowListModel);
        }
    }

    Component {
        id: rowButton

        Rectangle {
            id: button
            width: rowButtonsLayout.width / (rowListModel.count || 1) - (rowListModel.count ? rowButtonsLayout.spacing * (rowListModel.count - 1) : 0)
            height: 2 * buttonText.font.pixelSize
            color: active ? (rowPicker.followTransportColorScheme ? pageLayout.headerColor : pageLayout.colorPalete["headerBG"]) : "#ddd";
            
            property bool active: buttonActive || false

            Text {
                id: buttonText
                anchors.fill: parent
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                text: textContent
                elide: Text.ElideRight
                fontSizeMode: Text.HorizontalFit
                color: parent.active ? pageLayout.colorPalete["headerText"] : "#000"
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    Haptics.play();
                    if(rowListModel.count <= 1 || !rowListModel.get(index).buttonActive) {
                        var callback = rowPicker.hasCallback(index);
                        if(callback) {
                            callback(index);
                        }
                        rowPicker.selectItem(index);
                    }
                }
            }
        }
    }

    Item {
        anchors {
            left: parent.left
            right: parent.right
            topMargin: units.gu(2)
            bottomMargin: units.gu(2)
            leftMargin: rowPicker.leftRightMargins || units.gu(2)
            rightMargin: rowPicker.leftRightMargins || units.gu(2)
        }
        height: childrenRect.height + anchors.topMargin + anchors.bottomMargin

        ColumnLayout {
            anchors {
                left: parent.left
                right: parent.right
            }
            spacing: (rowPicker.narrow ? units.gu(0.5) : units.gu(2))

            Item {
                id: titleTextWrapper
                Layout.fillWidth: true
                height: titleText.contentHeight * (rowPicker.narrow ? 1.25 : 2)
                visible: titleText.text !== ""

                Label {
                    id: titleText
                    anchors.fill: parent
                    wrapMode: Text.WordWrap
                    verticalAlignment: (rowPicker.narrow ? Text.AlignBottom : Text.AlignVCenter)
                    font.pointSize: (rowPicker.narrow ? units.gu(1.25) : units.gu(1.75))
                    text: ""
                }
            }
            
            Label {
                id: footNoteText
                Layout.fillWidth: true
                wrapMode: Text.WordWrap
                verticalAlignment: Text.AlignVCenter
                font.pointSize: units.gu(1.35)
                text: ""
                visible: text !== ""
            }

            ListView {
                id: rowButtonsLayout
                Layout.fillWidth: true
                model: ListModel {
                    id: rowListModel
                }
                spacing: 1
                interactive: false
                orientation: Qt.Horizontal
                delegate: rowButton
            }

            Item {
                Layout.fillWidth: true
            }
        }
    }
}
