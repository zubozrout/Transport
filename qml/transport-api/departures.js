"use strict";

var Departures = function(parent, data) {
    this.parent = parent || {};
    this.data = data || {};
    this.id = this.data.id || null;
    this.details = this.data.data || {};
    this.statusOk = false;

    if(Object.keys(this.details).length > 0) {
        this.parseData();
    }

    return this;
}

Departures.prototype.parseData = function() {
    this.fromName = null;
    if(this.details.fromObjects && this.details.fromObjects.masks) {
        if(this.details.fromObjects.masks.length > 0) {
            this.fromName = this.details.fromObjects.masks[0].name;
        }
    }
    
    this.toName = null;
    if(this.details.toObjects && this.details.toObjects.masks) {
        if(this.details.toObjects.masks.length > 0) {
            this.toName = this.details.toObjects.masks[0].name;
        }
    }
    
    this.statusOk = this.fromName || this.toName;

    this.records = this.details.trains || [];
}

Departures.prototype.parseRecord = function(record) {
    var parsed = {};
    parsed.connectionsID = this.id || null;
    record.train = record.train || {};

    parsed.type = record.train.type;
    parsed.typeName = record.train.typeName;
    parsed.typeIndex = record.train.id;
    parsed.num = record.train.num1 || 0;
    parsed.name = record.train.num2 || "";
    parsed.color = record.train.color;
    parsed.flags = record.train.flags;

    parsed.from = {};
    parsed.from.name = this.fromName || "";
    parsed.from.exact = record.station1.name || "";

    var recordDate = dateStringtoDate(record.dateTime || record.dateTime1);
    parsed.from.date = recordDate;
    parsed.from.time = dateToTimeString(recordDate);

    parsed.to = {};
    parsed.to.name = record.destination || record.stationTrainEnd ? record.stationTrainEnd.name : "";
    
    var arrivalDate = (record.timeTrainEnd || "").replace(/^(\d{1})(\:.*)/g, "0$1$2");
    parsed.to.time = arrivalDate;

    parsed.delay = Number(record.delay) >= 0 ? record.delay : 0;
    parsed.delayQuery = record.delayQuery || null;

    parsed.fixedCodes = record.fixedCodes ? JSON.stringify(record.fixedCodes) : null;
    
    var directionArray = [];
    if(record.direction) {
        for(var i = 0; i < record.direction.length; i++) {
            direction.push(record.direction[i]);
        }
    }
    parsed.direction = directionArray.join(", ");
    
    parsed.notesArray = [];
    if(record.train.fixedCodes) {
        for(var i = 0; i < record.train.fixedCodes.length; i++) {
            parsed.notesArray.push(record.train.fixedCodes[i].desc);
        }
    }
    parsed.notes = parsed.notesArray.join(", ");
    
    return parsed;
}

