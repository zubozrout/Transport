import QtQuick 2.9

import "../transport-api.js" as Transport

Item {
    id: delayTimer
    property var callback: null
    property var conId: null
    property var query: null
    property bool enabled: false
    
    function start() {
        enabled = true;
    }
    
    onVisibleChanged: {
        if(visible && enabled) {
            // timerItself.triggeredOnStart = true;
            timerItself.restart();
        }
        else {
            // timerItself.triggeredOnStart = false;
            timerItself.stop();
        }
    }
    
    Timer {
        id: timerItself
        interval: 5000
        running: delayTimer.enabled
        repeat: true
        triggeredOnStart: false
        
        onTriggered: {
            if(typeof callback === "function" && conId && query) {
                var index = Number(Transport.transportOptions.getDBSetting("update-delays") || 0); // Only run if enabled in settings
                if(index <= 0) {
                    Transport.GeneralTranport.getCurrentDelay({
                        connectionsID: conId,
                        delayQuery: query,
                        callback: function(response) {
                            response = response || {};
                            callback && callback(response);
                            if(response.status === 500) {
                                delayTimer.enabled = false;
                                return false;
                            }
                            interval = 60000;
                        }
                    });
                }
            }
            else {
                delayTimer.enabled = false;
            }
        }
    }
}
