"use strict";

function langCode(toInt) {
    var locale = Qt.locale().name;
    var lang = "";
    var num = 0;

    switch(locale.substring(0,2)) {
        case "en":
            lang = "ENGLISH";
            num = 1;
            break;
        case "de":
            lang = "GERMAN";
            num = 2;
            break;
        case "cs":
            lang = "CZECH";
            num = 0;
            break;
        case "sk":
            lang = "SLOVAK";
            num = 3;
            break;
        case "pl":
            lang = "POLISH";
            num = 4;
            break;
        default:
            lang = "ENGLISH";
            num = 1;
            break;
    }

    if(toInt !== undefined && toInt === true) {
        return num;
    }
    return lang;
}

function getTranpsortType(index) {
    switch(index) {
    case 1:
       return "train";
    case 2:
        return "bus";
    case 3:
        return "tram";
    case 4:
       return "trol";
    case 5:
       return "metro";
    case 6:
       return "ship";
    case 7:
       return "air";
    case 8:
       return "taxi";
    case 9:
       return "cableway";
    default:
       return "empty";
    }
}

function dateStringtoDate(dateString) {
    if(!dateString) {
        return "";
    }

    const [ date, time ] = dateString.split(" ");

    const dateTime = {
        day: 0,
        month: 0,
        year: 0,
        hours: 0,
        minutes: 0
    }

    if(date) {
        const dateParts = date.split(".");
        dateTime.day = dateParts[0];
        dateTime.month = parseInt(dateParts[1]) - 1;
        dateTime.year = dateParts[2];
    }

    if(time) {
        const timeParts = time.split(":");
        dateTime.hours = timeParts[0];
        dateTime.minutes = timeParts[1];
    }

    const finalDate = new Date(dateTime.year, dateTime.month, dateTime.day, dateTime.hours, dateTime.minutes, 0, 0, {
        timeZone: "Europe/Prague"
    });
    return finalDate;
}

function dateToTimeString(date) {
    if(date) {
        date = new Date(date);

        var hours = String(date.getHours());
        var minutes = String(date.getMinutes());
        if(hours.length === 1) {
            hours = "0" + hours;
        }
        if(minutes.length === 1) {
            minutes = "0" + minutes;
        }
        return hours + ":" + minutes;
    }
    return "";
}

function dateToString(date) {
    if(date) {
        date = new Date(date);

        var day = date.getDate();
        var month = date.getMonth() + 1;
        var year = date.getFullYear();

        return day + "." + month + "." + year + " " + dateToTimeString(date);
    }
    return "";
}

function lineColor(typeId, line) {
    switch(typeId) {
        case 5: //subway
            switch(line.toLowerCase()) {
                case "a":
                    return "#1ba300";
                case "b":
                    return "#ffd432";
                case "c":
                    return "#ff2730";
                case "d":
                    return "#1A237E";
            }
            break;
        case 1: // train
            return "#373d68";
        case 3: // tram
            return "#7a0603";
        case 4: // trolleybus
            return "#9f367a";
        case 6: // ship
            return "#00b3cb";
        case 9: // cableway
            return "#bad147";
        default:
            return "#000000";
    }
}

function setStopData({ Stop, dbConnection, stopSearch, stopidfrom, stopnamefrom, typeid }) {
    stopSearch.setData({
        selectedStop: new Stop({
            id: stopidfrom,
            item: {
                name: stopnamefrom
            }
        }, {
            transportID: typeid,
            dbConnection
        }),
        value: stopnamefrom
    });

    /* Ehm ..
    console.log("Transport", typeof Transport, Transport);
        > Transport object undefined
    
        ...stupid QML and its obsolete and problematic JS :(
    */
    /*
    stopSearch.setData({
        selectedStop: new (typeof Transport !== typeof undefined ? Transport.Stop : Stop)({
            id: stopidfrom,
            item: {
                name: stopnamefrom
            }
        }, {
            transportID: typeid,
            dbConnection: (typeof Transport !== typeof undefined ? Transport.transportOptions.dbConnection : transportOptions.dbConnection)
        }),
        value: stopnamefrom
    });
    */
}

// SOURCE: http://stackoverflow.com/a/21623206/1642887
function latLongDistance(lat1, lon1, lat2, lon2) {
  var p = Math.PI / 180;
  var c = Math.cos;
  var a = 0.5 - c((lat2 - lat1) * p)/2 + c(lat1 * p) * c(lat2 * p) * (1 - c((lon2 - lon1) * p))/2;

  return 12742 * Math.asin(Math.sqrt(a)); // 2 * R; R = 6371 km
}

/* Fixed, CET vs UTC, 2hrs away in summer, 1hr in winter
* Compare to current date offset to UTC.
* This will only work reliably in the ~EU,
* but for the purpose of this app this should be ok-ish since QML doesn't allow us working with timezones properly. */
const cachedDateCETOffset = (() => {
    const currentDate = new Date();
    const currentDateUTCOffset = currentDate.getTimezoneOffset(); /* In minutes */

    const currentYear = new Date().getFullYear();
    const winterTimezoneOffset = new Date(currentYear, 0, 1).getTimezoneOffset();
    const summerTimezoneOffset = new Date(currentYear, 6, 1).getTimezoneOffset();

    const winterTime = currentDateUTCOffset === winterTimezoneOffset;
    const summerTime = currentDateUTCOffset === summerTimezoneOffset;

    const winterTimeOffset = -60;
    const summerTimeOffset = -120;

    let cetDateUTCOffset = winterTime ? winterTimeOffset : summerTimeOffset;
    if(winterTime === summerTime) {
        /* We are in a timezone that has fixed time, no daylight saving ... use a very rough estimate to calculate diff based on dates. */
        cetDateUTCOffset = (currentDate.getMonth() < 4 || currentDate.getMonth() > 10) ? winterTimeOffset : summerTimeOffset;
    }

    return cetDateUTCOffset - currentDateUTCOffset;
})();

function remainingTimeLabel(toDate) {
    const currentDate = new Date();
    const currentDateShifted = new Date(currentDate.setMinutes(currentDate.getMinutes() - cachedDateCETOffset));
    currentDateShifted.setMilliseconds(0);
    
    const delta = (toDate - currentDateShifted) / 1000; /* The begining of the minute */
    const offsetDelta = delta + 59; /* Hold on till the end of the minute + 59 seconds */

    if(currentDateShifted - (1000 * 59) <= toDate) {
        const days = Math.floor(offsetDelta / 3600 / 24);
        const hours = Math.floor(offsetDelta / 3600) % 24;
        const minutes = Math.floor(offsetDelta / 60) % 60;
        const seconds = Math.floor(offsetDelta) % 60;

        if(days <= 0) {
            if(hours < 1) {
                if(minutes <= 0) {
                    if(seconds > 30) {
                        return {
                            text: i18n.tr("in less than a minute"),
                            inFuture: true
                        }
                    }
                    return {
                        text: i18n.tr("now"),
                        inFuture: true
                    }
                }
                return {
                    text: i18n.tr("in %1 minute", "in %1 minutes", minutes).arg(minutes),
                    inFuture: true
                }
            }
            const prependZero = num => `0${num}`.slice(-2);
            return {
                text: `${prependZero(hours)}:${prependZero(minutes)}:${prependZero(seconds)}`,
                inFuture: true
            }
        }
        return {
            text: i18n.tr("in %1 day", "in %1 days", days).arg(days),
            inFuture: true
        }
    }
    return {
        text: i18n.tr("departed"),
        inFuture: false
    }
}

/* https://stackoverflow.com/a/15886205 */
/* Replace accents with ascii */
function accentFold(str) {
    return str.replace(/([àáâãäå])|([çčć])|([èéêë])|([ìíîï])|([ñ])|([òóôõöø])|([ß])|([ùúûü])|([ÿ])|([æ])/g,
        (str, a, c, e, i, n, o, s, u, y, ae) => {
            if(a) return 'a';
            if(c) return 'c';
            if(e) return 'e';
            if(i) return 'i';
            if(n) return 'n';
            if(o) return 'o';
            if(s) return 's';
            if(u) return 'u';
            if(y) return 'y';
            if(ae) return 'ae';
        }
    );
}

/* Invert color https://stackoverflow.com/a/35970186/1642887 */
function invertColor(hex, bw) {
    if(hex.startsWith("#")) {
        hex = hex.slice(1);
    }
    // convert 3-digit hex to 6-digits.
    if(hex.length === 3) {
        hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
    }
    if(hex.length !== 6) {
        throw new Error("Invalid HEX color.");
    }
    let r = parseInt(hex.slice(0, 2), 16),
        g = parseInt(hex.slice(2, 4), 16),
        b = parseInt(hex.slice(4, 6), 16);
    if(bw) {
        // https://stackoverflow.com/a/3943023/112731
        return (r * 0.299 + g * 0.587 + b * 0.114) > 186 ? "#000000" : "#ffffff";
    }
    // invert color components
    r = (255 - r).toString(16);
    g = (255 - g).toString(16);
    b = (255 - b).toString(16);
    // pad each with zeros and return
    return "#" + padZero(r) + padZero(g) + padZero(b);
}