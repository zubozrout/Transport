import QtQuick 2.9
import QtQuick.Layouts 1.3
import Lomiri.Components 1.3

RowLayout {
    id: rowSwitchLayout
    spacing: units.gu(2)
    
    property bool silent: false
    property int count: 1
    property int currentIndex: 0
    
    function updatePosition() {
        if(currentIndex >= 0 && currentIndex < count) {
            x = -(currentIndex * parent.width + currentIndex * spacing);
        }
        else {
            x = 0;
        }
    }
    
    onCurrentIndexChanged: updatePosition();
    
    Behavior on x {
        enabled: !rowSwitchLayout.silent
        
        NumberAnimation {
            duration: 500
            easing.type: Easing.InOutBack
        }
    }
}
