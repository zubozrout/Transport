import QtQuick 2.9
import Lomiri.Components 1.3 as UITK
import QtQuick.Layouts 1.3

ColumnLayout {
    id: connectionNotesList

    Layout.fillWidth: true
    Layout.topMargin: units.gu(2)
    Layout.leftMargin: units.gu(2)
    Layout.rightMargin: units.gu(2)
    spacing: units.gu(1)
    visible: routeNotesModel.count > 0
    height: routeNotesModel.count > 0 ? implicitHeight : 0
    clip: true

    property var notesModel: routeNotesModel

    states: [
        State {
            name: "visible"
            PropertyChanges {
                target: routeNotesView
                implicitHeight: routeNotesView.contentHeight
            }
        },
        State {
            name: "hidden"
            PropertyChanges {
                target: routeNotesView
                implicitHeight: 0
            }
        }
    ]
    state: "hidden"

    Canvas {
        Layout.fillWidth: true
        Layout.preferredHeight: units.gu(0.25)

        onPaint:{
            var ctx = getContext("2d");

            ctx.setLineDash([width / 200, width / 200]);
            ctx.lineWidth = units.gu(0.25);

            ctx.beginPath();
            ctx.moveTo(0, units.gu(0.25) / 2);
            ctx.lineTo(width, units.gu(0.25) / 2);
            ctx.strokeStyle = pageLayout.colorPalete["baseAlternateText"];
            ctx.stroke();
        }
    }

    Item {
        Layout.fillWidth: true
        Layout.preferredHeight: childrenRect.height
        Layout.leftMargin: units.gu(2)
        Layout.rightMargin: units.gu(2)

        RowLayout {
            width: parent.width
            spacing: units.gu(2)

            UITK.Label {
                Layout.fillWidth: true
                text: connectionNotesList.state === "visible" ? i18n.tr("Hide notes") : i18n.tr("Show notes")
                font.pixelSize: FontUtils.sizeToPixels("normal")
                font.bold: true
            }

            UITK.Icon {
                Layout.fillWidth: false
                Layout.preferredWidth: units.gu(2)
                Layout.preferredHeight: width
                color: pageLayout.colorPalete["baseText"]
                name: connectionNotesList.state === "visible" ? "up" : "down"
            }
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(connectionNotesList.state === "visible") {
                    connectionNotesList.state = "hidden";
                    return;
                }
                connectionNotesList.state = "visible";
                return;
            }
        }
    }

    ListView {
        id: routeNotesView
        Layout.fillWidth: true
        Layout.leftMargin: units.gu(2)
        Layout.rightMargin: units.gu(2)
        interactive: false
        implicitHeight: contentHeight
        clip: true
        spacing: units.gu(0.5)

        delegate: Component {
            RowLayout {
                anchors {
                    left: parent.left
                    right: parent.right
                }
                spacing: units.gu(0.5)

                UITK.Label {
                    text: key
                    font.pixelSize: FontUtils.sizeToPixels("small")
                    font.bold: true
                    wrapMode: Text.WordWrap

                    Layout.fillWidth: false
                    Layout.preferredWidth: units.gu(5)
                    Layout.alignment: Qt.AlignLeft
                }

                UITK.Label {
                    text: value
                    font.pixelSize: FontUtils.sizeToPixels("small")
                    font.bold: false
                    wrapMode: Text.WordWrap

                    Layout.fillWidth: true
                    Layout.alignment: Qt.AlignLeft
                }
            }
        }
        
        model: ListModel {
            id: routeNotesModel
        }
    }
}
