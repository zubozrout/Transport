import QtQuick 2.9
import QtQuick.Layouts 1.3
import Lomiri.Components 1.3

Column {
    id: dateTimeSelector
    anchors {
        left: parent.left
        right: parent.right
    }
    spacing: units.gu(2)
    
    property bool customDate: customDateSwitch.checked
    property var date: Qt.formatDate(datetimePicker.datePicker.date, "d.M.yyyy")
    property var time: Qt.formatTime(datetimePicker.timePicker.date, "hh:mm")
    property var dateTime: dateTimeSelector.date + " " + dateTimeSelector.time
    
    RowLayout {
        width: parent.width
        spacing: units.gu(2)
        anchors {
            left: parent.left
            right: parent.right
        }

        Label {
            text: i18n.tr("Custom date")
            wrapMode: Text.WordWrap
            Layout.fillWidth: true
        }

        Switch {
            id: customDateSwitch
            checked: false
            Layout.fillWidth: false
        }
    }

    DateTimePicker {
        id: datetimePicker
        visible: customDateSwitch.checked
    }
}
