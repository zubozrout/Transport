import QtQuick 2.9
import QtQuick.Layouts 1.3
import Lomiri.Components 1.3 as UITK

ColumnLayout {
    id: menu
    visible: shouldBeVisible
    spacing: 0
    
    property bool shouldBeVisible: false
    
    function toggle() {
        shouldBeVisible = !shouldBeVisible;
        if(shouldBeVisible) {
            focus = true;
        }
    }
    
    function close() {
        shouldBeVisible = false;
    }
    
    onFocusChanged: {
        if(!focus) {
            close();
        }
    }
}
