# Transport

Ubuntu Phone Transport Application v2 - Czech and Slovak public transport schedules + European trains + Airlines.

This is a Ubuntu Phone application for searching connections for public transport - mostly in the Czech and Slovak republic.
It uses API provided by CHAPS s.r.o. company.

Please note I can't share the private key used by this app - therefore using the code present in this repository will only allow you to use free API.
The real Ubuntu Phone app present in the store here https://open-store.io/app/transport.zubozrout contains the private key.

Here is the CHAPS API documentation: http://docs.crws.apiary.io/#reference

Please help translate this app here: https://www.transifex.com/zubozrout/transport-ng/

## Pages

You can find Transport app web page here: https://zubozrout.gitlab.io/Transport

## Building for desktop

`clickable desktop`

## Building for phone

- `clickable --arch="arm64"`
- `clickable --arch="armhf"`

## Prerequisites
You need to have this file in place (not part of this repository):

`./plugins/Api/key.h
`

containing the following line:

`QString KEY = "";`

(You can fill in the API key provided by CHAPS s.r.o. for extended functionality or leave the string empty as above)

## Download the latest GitLab builds

You may also install Transport directly as a click package. One place to download it is right here on GitLab.

The CI is set up to build UT Focal (Ubuntu 20.04) click packages for the following architectures:
- [Transport (arm64)](https://gitlab.com/zubozrout/Transport/-/jobs/artifacts/master/file/build/aarch64-linux-gnu/app/transport.zubozrout_arm64.click?job=build-arm64)
- [Transport (armhf)](https://gitlab.com/zubozrout/Transport/-/jobs/artifacts/master/file/build/arm-linux-gnueabihf/app/transport.zubozrout_armhf.click?job=build-armhf)
- [Transport (amd64)](https://gitlab.com/zubozrout/Transport/-/jobs/artifacts/master/file/build/x86_64-linux-gnu/app/transport.zubozrout_amd64.click?job=build-amd64)

If you are unsure, your mobile device is most likely relying on an arm64 system build.
