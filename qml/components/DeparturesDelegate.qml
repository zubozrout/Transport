import QtQuick 2.9
import Lomiri.Components 1.3
import QtQuick.Layouts 1.1
import QtGraphicalEffects 1.0

import "../generalfunctions.js" as GeneralFunctions

Component {
    id: departuresDelegate
    
    Rectangle {
        id: departuresRectangle
        anchors {
            left: parent.left
            right: parent.right
        }
        height: visible ? departureItem.height + 2 * departureItem.anchors.margins : 0
        color: index % 2 === 0 ? "transparent" : pageLayout.colorPalete["secondaryBG"]

        Item {
            id: departureItem
            anchors {
                left: parent.left
                right: parent.right
                margins: units.gu(2)
                verticalCenter: parent.verticalCenter
            }
            height: childrenRect.height

            RowLayout {
                id: departureRow
                anchors {
                    left: parent.left
                    right: parent.right
                }
                spacing: units.gu(2)

                RowLayout {
                    Layout.fillWidth: false
                    Layout.fillHeight: true
                    Layout.minimumWidth: units.gu(7)
                    Layout.preferredWidth: units.gu(7)
                    spacing: units.gu(0.05)
                    
                    TransportIcon {
                        id: transportTypeIconItem
                        Component.onCompleted: {
                            setData(typeIndex, num);
                        }
                        Layout.minimumWidth: units.gu(3)
                        Layout.preferredWidth: units.gu(3)
                        Layout.fillWidth: false
                        Layout.alignment: Qt.AlignLeft
                    }

                    Label {
                        text: num
                        fontSizeMode: Text.Fit
                        font.pixelSize: FontUtils.sizeToPixels("large")
                        font.bold: true
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        wrapMode: Text.WordWrap

                        Layout.fillWidth: true
                        Layout.alignment: Qt.AlignCenter
                    }
                }
                
                ColumnLayout {
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    spacing: units.gu(0.5)

                    GridLayout {
                        Layout.fillWidth: true
                        Layout.fillHeight: true
                        columnSpacing: units.gu(1)
                        columns: 2
                        
                        Label {
                            text: (from.exact && from.time) ? from.time : ""
                            font.pixelSize: FontUtils.sizeToPixels("medium")
                            font.bold: false
                            wrapMode: Text.NoWrap
                            visible: text !== ""

                            Layout.fillWidth: false
                            Layout.preferredWidth: units.gu(4)
                            Layout.alignment: Qt.AlignTop
                        }

                        Label {
                            text: from.exact ? i18n.tr("From") + ": " + from.exact : ""
                            font.pixelSize: FontUtils.sizeToPixels("medium")
                            font.bold: false
                            wrapMode: Text.WordWrap
                            visible: text !== ""

                            Layout.fillWidth: true
                            Layout.alignment: Qt.AlignTop
                        }
                        
                        Label {
                            text: to.time ? to.time : ""
                            font.pixelSize: FontUtils.sizeToPixels("medium")
                            font.bold: false
                            wrapMode: Text.NoWrap
                            visible: text !== ""

                            Layout.fillWidth: false
                            Layout.preferredWidth: units.gu(4)
                            Layout.alignment: Qt.AlignTop
                        }
                        
                        Label {
                            text: i18n.tr("To") + ": " + to.name
                            font.pixelSize: FontUtils.sizeToPixels("medium")
                            font.bold: false
                            wrapMode: Text.WordWrap

                            Layout.fillWidth: true
                            Layout.alignment: Qt.AlignTop
                        }
                    }
                    
                    Label {
                        text: direction ? (i18n.tr("Via") + ": " + direction) : ""
                        font.pixelSize: FontUtils.sizeToPixels("small")
                        font.bold: false
                        wrapMode: Text.WordWrap
                        visible: text !== ""

                        Layout.fillWidth: true
                        Layout.alignment: Qt.AlignLeft
                    }
                    
                    Label {
                        id: delayLabel
                        text: delay ? i18n.tr("%1 minute delay", "%1 minutes delay", delay).arg(delay) : ""
                        font.pixelSize: FontUtils.sizeToPixels("small")
                        font.bold: true
                        color: upToDate ? pageLayout.colorPalete["warningText"] : pageLayout.colorPalete["secondaryText"]
                        wrapMode: Text.WordWrap
                        visible: text !== ""
                        
                        property bool upToDate: true

                        Layout.fillWidth: true
                        Layout.alignment: Qt.AlignLeft
                    }
                    
                    Label {
                        text: notes || ""
                        font.pixelSize: FontUtils.sizeToPixels("small")
                        font.bold: false
                        wrapMode: Text.WordWrap
                        visible: text !== ""

                        Layout.fillWidth: true
                        Layout.alignment: Qt.AlignLeft
                    }
                }
                
                ColumnLayout {
                    spacing: units.gu(0.5)
                    Layout.fillWidth: false
                    Layout.fillHeight: true
                    Layout.alignment: Qt.AlignTop

                    Label {
                        id: timeToLabel
                        text: "N/A"
                        font.pixelSize: FontUtils.sizeToPixels("small")
                        font.bold: true
                        horizontalAlignment: Text.AlignRight
                        verticalAlignment: Text.AlignTop
                        wrapMode: Text.NoWrap

                        Layout.fillHeight: false
                        Layout.minimumWidth: contentWidth
                        Layout.alignment: Qt.AlignRight
                    }

                    PlatformLabel {
                        platform: fixedCodes ? JSON.parse(fixedCodes).find(({ desc }) => desc.includes("nástupiště")) : null
                        alignRight: true
                    }

                    Item {
                        Layout.fillHeight: true
                    }
                }
            }
        }
        
        Timer {
            id: clockTimer
            running: true
            triggeredOnStart: true
            interval: 1000
            repeat: true
            onTriggered: {
                if(from.date) {
                    var departureTimePlusDelay = new Date(from.date.getTime() + (delay * 60000))
                    var result = GeneralFunctions.remainingTimeLabel(departureTimePlusDelay);
                    timeToLabel.text = result.text;
                    if(result.inFuture) {
                        timeToLabel.color = pageLayout.colorPalete["successText"];
                    }
                    else {
                        timeToLabel.color = pageLayout.colorPalete["warningText"];
                        repeat = false;
                        
                        //departuresRectangle.opacity = 0.25;
                        //hideTimer.start();
                    }
                }
            }
        }
        
        Timer {
            id: hideTimer
            running: false
            triggeredOnStart: false
            interval: 36000
            repeat: false
            onTriggered: {
                departuresRectangle.visible = false;
            }
        }
        
        DelayTimer {
            id: delayTimer
            enabled: true
            conId: connectionsID
            query: typeof delayQuery !== typeof undefined ? delayQuery : null
            callback: function(response) {
                response = response || {};
                if(typeof response.delay !== typeof undefined) {
                    delay = response.delay;
                    delayLabel.upToDate = true;
                    clockTimer.restart();
                }
                else {
                    delayLabel.upToDate = false;
                }
            }
        }
    }
}
