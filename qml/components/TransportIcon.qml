import QtQuick 2.9
import Lomiri.Components 1.3
import QtQuick.Layouts 1.1
import QtGraphicalEffects 1.0

import "../generalfunctions.js" as GeneralFunctions

Item {
    id: transportTypeIconItem
    anchors.margins: width / 10
    
    Layout.fillWidth: true
    Layout.fillHeight: true
    Layout.minimumHeight: transportTypeIcon.height + 2 * anchors.margins
    Layout.alignment: Qt.AlignCenter
    
    property var lineTypeId: null
    property var lineNumber: null
    property var lineColor: null
    
    function setData(typeId, number) {
        lineTypeId = typeId;
        lineNumber = number;
        if(typeId && number) {
            lineColor = GeneralFunctions.lineColor(typeId, number);
        }
    }
    
    Rectangle {
        anchors {
            top: parent.top
            right: parent.left
            bottom: parent.bottom
            margins: parent.width / 8
        }
        width: parent.width / 8
        color: lineColor !== "#000000" ? lineColor : "transparent"
    }

    Image {
        id: transportTypeIcon
        anchors.centerIn: transportTypeIconItem
        width: parent.width - 2 * parent.anchors.margins
        height: width
        fillMode: Image.PreserveAspectFit
        horizontalAlignment: Image.AlignHCenter
        verticalAlignment: Image.AlignVCenter
        sourceSize: Qt.size(width, height)
        source: {
            const iconPath = parent.lineTypeId ? "icons/" + GeneralFunctions.getTranpsortType(parent.lineTypeId) + ".svg" : "icons/empty.svg";
            return Qt.resolvedUrl("qrc:/" + iconPath);
        }
    }
    
    ColorOverlay {
        anchors.fill: transportTypeIcon
        source: transportTypeIcon
        color: pageLayout.colorPalete["baseText"]
        cached: true
    }
}
