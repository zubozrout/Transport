import QtQuick 2.9
import Lomiri.Components 1.3
import QtQuick.Layouts 1.2
import Lomiri.Components.Popups 1.3

import "../components"

import "../transport-api.js" as Transport
import "../generalfunctions.js" as GeneralFunctions

Page {
    id: departuresPage
    header: PageHeader {
        id: pageHeader
        title: i18n.tr("Station departures")

        StyleHints {
            foregroundColor: pageLayout.colorPalete["headerText"]
            backgroundColor: pageLayout.headerColor || pageLayout.colorPalete["headerBG"]
        }
    }
    
    clip: true
    
    property var lastData: null
    
    function updateView(limitTo) {
        if(lastData) {
            var departures = lastData.departures || {};
            if(!departures.records) {
                return [];
            }
            
            departuresModel.clear();

            var lines = [];
            for(var i = 0; i < departures.records.length; i++) {
                var record = departures.records[i];
                var parsed = departures.parseRecord(record);
                lines.push(parsed.num);
                
                if(limitTo !== null && parsed.num !== limitTo) {
                    continue;
                }
                
                departuresModel.append(parsed);
            }

            pageHeader.title = i18n.tr("Station") + ": " + (departures.fromName || departures.toName);
            departuresFlickable.contentY = 0;

            function onlyUnique(value, index, self) { 
                return self.indexOf(value) === index;
            }

            return lines.filter(onlyUnique);
        }
        return [];
    }

    function loadList(data, limitTo) {
        data = data || {};
        lastData = data;
        
        var lines = updateView(null);

        linesPicker.clear();
        linesPicker.append({
            textContent: i18n.tr("All")
        });
        linesPicker.selectItem(0);

        function sortNumber(a, b) {
            if(a.match(/\D/) && b.match(/\D/)) {
                return a < b;
            }
            if(a.match(/\D/)) {
                return -1;
            }
            return a - b;
        }

        lines.sort(sortNumber);
        for(var i = 0; i < lines.length; i++) {
            if(!linesPicker.hasValue(lines[i])) {
                linesPicker.append({
                    textContent: lines[i]
                });
            }
        }

        linesPickerWrapper.visible = lines.length > 1;
    }

    DeparturesDelegate {
        id: departuresDelegate
    }

    Flickable {
        id: departuresFlickable
        anchors {
            top: pageHeader.bottom
            right: parent.right
            bottom: linesPickerWrapper.top
            left: parent.left
        }
        contentWidth: parent.width
        contentHeight: departuresView.contentHeight + 2 * departuresView.anchors.margins
        flickableDirection: Flickable.VerticalFlick

        clip: true
        
        ListView {
            id: departuresView
            anchors.fill: parent
            height: departuresModel.height
            
            interactive: false
            delegate: departuresDelegate

            model: ListModel {
                id: departuresModel
            }
        }
    }
    
    ColumnLayout {
        id: linesPickerWrapper
        anchors {
            right: parent.right
            bottom: parent.bottom
            left: parent.left
        }
        height: visible ? undefined : 0
        spacing: 0
        
        Rectangle {
            Layout.fillWidth: true
            Layout.preferredHeight: units.gu(1)
            color: pageLayout.headerColor
        }

        RowPicker {
            id: linesPicker
            Layout.fillWidth: true
            Layout.topMargin: units.gu(2)
            Layout.bottomMargin: units.gu(2)
            anchors {
                left: undefined
                right: undefined
            }

            property bool departure: true
            property bool followTransportColorScheme: true
            property var render: function(model) {
                clear();
                initialize([], 0, function(itemIndex) {
                    departuresPage.updateView(itemIndex === 0 ? null : linesPicker.indexData(itemIndex).textContent);
                });
            }

            Component.onCompleted: {
                setTite(i18n.tr("Filter by line"), true);
                linesPicker.update(function(model) { linesPicker.render(model) });
            }
        }
    }

    Scrollbar {
        flickableItem: departuresFlickable
        align: Qt.AlignTrailing
    }
}
