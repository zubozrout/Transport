"use strict";

var TransportOptions = function({
	id = "id",
	transportsData = {},
	transports = [],
	callback = () => {},
	dbConnection = {},
	eventListener = {}, /* Updates UI on local changes */
	transportOptionPrefix = "transportOption"
}) {
	console.log("NEW TransportOptions", "dbConnection", dbConnection);
	this.id = id;
	this.transportsData = transportsData;
	this.transports = transports;
	this.callback = callback;
	this.dbConnection = dbConnection;
	this.eventListener = eventListener;
	this.transportOptionPrefix = transportOptionPrefix;
	this.selectedIndex = -1;
	
	return this;
}

TransportOptions.prototype.clearAll = function(database) {
	if(database === true) {
		this.clearDbData();
	}
	this.clearCache();
};

/* Remove all data from cache */
TransportOptions.prototype.clearCache = function() {
	console.log("Clearing runtime cached data...");
	this.transportsData = {};
	this.transports = [];

	this.callback(this);
};

/* Remove all data from the DB */
TransportOptions.prototype.clearDbData = function() {
	console.log("Clearing app data...");
	console.log("Dropping tables...");
	this.dbConnection.dropTables();
	console.log("Re-creating tables...");
	this.dbConnection.createTables();
	this.callback(this);
};

/* TODO: Why is this even here? */
TransportOptions.prototype.refreshLocal = function() {
	console.log("Calling update info callback...");
	this.callback(this);
};

TransportOptions.prototype.fetchTrasports = function({ forceServer = false, callback = () => {} }) {
	const transportOptions = this.dbConnection.getDataJSON("transportOptions") || null;
	const checkFrequencyNumber = Number(this.getDBSetting("check-frequency"));
	
	let checkServer = !transportOptions;
	switch(checkFrequencyNumber) {
		case 3:
			// never
			checkServer = false;
			break;
		case 2:
			// everytime
			checkServer = true;
			break;
		case 1:
			// daily
			checkServer = checkServer || GeneralTranport.dateOlderThan(new Date(transportOptions.date), new Date(), "day");
			break;
		default:
			// weekly
			checkServer = checkServer || GeneralTranport.dateOlderThan(new Date(transportOptions.date), new Date(), "week");
	}

	if(forceServer || checkServer) {
		this.fetchDBTransports({
			transportOptions,
			callback: (obj, status) => {
				callback(obj, status);
				this.fetchServerTransports({ callback });
			}
		});
	}
	else {
		this.fetchDBTransports({
			transportOptions,
			callback
		});
	}
};

TransportOptions.prototype.trySelectingTransportIndexFromHistory = function() {
	const searchHistory = this.dbConnection.getSearchHistory();
	if(searchHistory.length) {
		const previouslySelectedId = searchHistory[0].typeid;
		if(!this.selectTransportById(previouslySelectedId)) {
			this.selectIndex(-1);
		}
	}
	else {
		const dbIndexEntry = this.getDBSetting("latest-selected-transport");
		const previouslySelectedId = Number(dbIndexEntry !== null && dbIndexEntry !== false ? dbIndexEntry : -1);
		if(!this.selectIndex(previouslySelectedId)) {
			this.selectIndex(-1);
		}
	}
}

TransportOptions.prototype.fetchDBTransports = function({ transportOptions, callback = () => {} }) {
	if(transportOptions) {
		console.log("Fetching local DB transport info ...");
		this.transportsData = GeneralTranport.stringToObj(transportOptions.value);
		this.parseAllTransports();
		this.trySelectingTransportIndexFromHistory();
	}

	this.callback(this);
	callback(this);
};

TransportOptions.prototype.fetchServerTransports = function({ callback = () => {} }) {
	console.log("Fetching server-side (CHAPS s.r.o.) transport info ...");
	const self = this;
	this.request = GeneralTranport.getContent("https://ext.crws.cz/api/", (response) => {
		if(response && response.data) {
			self.dbConnection.saveDataJSON("transportOptions", response.data);
			self.transportsData = GeneralTranport.stringToObj(response.data);
			self.parseAllTransports();
			self.trySelectingTransportIndexFromHistory();
			self.callback(self, "SUCCESS");
			callback(self, "SUCCESS");
		}
		else {
			self.callback(self, "FAIL");
			callback(self, "FAIL");
		}
	});
};

TransportOptions.prototype.parseAllTransports = function() {
	const self = this;
	this.transports.length = 0;
	(this.transportsData.data || []).forEach(transportData => {
		const newTransportOption = new TransportOption({
			raw: transportData,
			dbConnection: self.dbConnection,
			eventListener: self.eventListener
		});
		self.transports.push(newTransportOption);

		const workingID = newTransportOption.getId();
		const newConnectionData = transportData;
		const oldConnectionDataSource = self.dbConnection.getDataJSON(`${this.transportOptionPrefix}-${workingID}`);

		if(oldConnectionDataSource) {
			const oldConnectionData = GeneralTranport.stringToObj(oldConnectionDataSource.value);
			if(oldConnectionData.ttValidFrom !== newConnectionData.ttValidFrom || oldConnectionData.ttValidTo !== newConnectionData.ttValidTo) {
				self.saveNewConnectionInfo({ id: workingID, newConnectionData });
			}
		}
		else {
			self.saveNewConnectionInfo({ id: workingID, newConnectionData });
		}
	});
	console.log(this.transports.length + " transport options were parsed.");
};

TransportOptions.prototype.saveNewConnectionInfo = function({ id, newConnectionData }) {
	this.dbConnection.saveDataJSON(`${this.transportOptionPrefix}-${id}`, JSON.stringify({
		id: newConnectionData.id,
		loaded: newConnectionData.loaded,
		name: newConnectionData.name,
		nameExt: newConnectionData.nameExt,
		ttValidFrom: newConnectionData.ttValidFrom,
		ttValidTo: newConnectionData.ttValidTo
	}));
};

TransportOptions.prototype.selectIndex = function(index) {
	if(this.transports.length > index && index >= 0 && this.selectedIndex !== index) {
		this.selectedIndex = index;
		this.saveDBSetting("latest-selected-transport", index);
		this.eventListener.update({
			name: "transport-change",
			response: () => this.getSelectedTransport()
		});
		return true;
	}
	return false;
};

TransportOptions.prototype.getTransportIndexById = function(id) {
	return this.transports.findIndex(transport => transport.id === id);
};

TransportOptions.prototype.getTransportById = function(id) {
	const index = this.getTransportIndexById(id);
	if(index >= 0) {
		return this.transports[index];
	}
	return null;
};

TransportOptions.prototype.selectTransportById = function(id) {
	const index = this.getTransportIndexById(id);
	if(index >= 0) {
		this.selectIndex(index);
		return this.getSelectedTransport();
	}
	return false;
};

/* TODO: Is this one really needed? */
TransportOptions.prototype.getSelectedIndex = function() {
	return this.selectedIndex;
};

TransportOptions.prototype.getSelectedId = function() {
	if(this.transports.length && this.selectedIndex >= 0) {
		return this.transports[this.selectedIndex].getId();
	}
	return null;
};

TransportOptions.prototype.getSelectedTransport = function() {
	if(this.selectedIndex >= 0) {
		return this.transports[this.selectedIndex];
	}
	return null;
};

/* Overwrite callback */
TransportOptions.prototype.setTransportUpdateCallback = function(callback) {
	this.callback = callback;
};

/* Abort fetch request */
TransportOptions.prototype.abort = function() {
	if(this.request) {
		this.request.abort();
		this.callback(this, "ABORT");
	}
};

/* FALLBACK */
TransportOptions.prototype.searchSavedStationsByLocation = function(cords, limit) {
	this.searchSavedStationsByLocationN({ cords, limit });
};

TransportOptions.prototype.searchSavedStationsByLocationN = function({ cords, limit = 10 }) {
	const dbStops = cords ? this.dbConnection.getNearbyStops(cords, limit) : [];
	return dbStops.map((dbStop, index) => {
		const transformData = {
			id: dbStop.id,
			item: {
				item: dbStop.item,
				name: dbStop.value,
				listId: dbStop.listId,
				coorX: dbStop.coorX,
				coorY: dbStop.coorY
			}
		}
		
		console.log("local", dbStop.value, dbStop.id);
		
		const stop = new Stop(transformData, {
			transportID: dbStop.key,
			dbConnection: this.dbConnection
		});
		return stop;
	});
};

/* FALLBACK */
TransportOptions.prototype.getNearbyStopsOnline = function(coords, limit, callback) {
	this.getNearbyStopsOnlineN({ coords, limit, callback });
};

TransportOptions.prototype.getNearbyStopsOnlineN = function({ coords, limit, callback = () => {} }) {
	const transport = this.getSelectedTransport();
	if(transport) {
		return transport.getNearbyStopsOnline(coords, limit, callback); /* TODO: transform params to object */
	}
	else {
		callback(false);
	}
	return false;
};

/* Database Settings manipulation */
TransportOptions.prototype.getDBSetting = function(key) {
	return this.dbConnection.getSetting(key);
};

TransportOptions.prototype.saveDBSetting = function(key, value) {
	if(key) {
		console.log("Saving setting to DB", key, value);
		return this.dbConnection.saveSetting(key, value);
	}
};
