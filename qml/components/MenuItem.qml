import QtQuick 2.9
import Lomiri.Components 1.3 as UITK

Rectangle {
    id: item
    anchors {
        margins: units.gu(1)
    }
    width: icon.width + label.width
    height: {
        var largestChild = icon.height > label.height ? icon.height : label.height;
        return units.gu(5) > largestChild ? units.gu(5) : largestChild;
    }
    color: pageLayout.colorPalete["baseBG"]
    border.width: 1
    border.color: pageLayout.colorPalete["baseAlternateText"]
    visible: enabled
    
    property alias icon: icon.name
    property alias text: label.text
    property bool enabled: true
    
    signal triggered
    
    UITK.Icon {
        id: icon
        anchors {
            left: parent.left
            verticalCenter: parent.verticalCenter
            margins: units.gu(1)
        }
        width: units.gu(2)
        height: width
        visible: icon.name != ""
        color: pageLayout.colorPalete["baseText"]
    }
    
    UITK.Label {
        id: label
        anchors {
            top: parent.top
            bottom: parent.bottom
            left: icon.right
            margins: units.gu(1)
        }
        width: units.gu(18)
        text: ""
        color: pageLayout.colorPalete["baseText"]
        verticalAlignment: Text.AlignVCenter
        wrapMode: Text.Wrap
    }
    
    MouseArea {
        id: mouseArea
        anchors.fill: parent
        
        onClicked: {
            item.triggered();
            item.parent.focus = false;
        }
        
        onPressed: {
            item.color = pageLayout.colorPalete["secondaryBG"];
        }
        
        onReleased: {
            item.color = pageLayout.colorPalete["baseBG"];
        }
    }
}
