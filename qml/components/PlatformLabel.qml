import QtQuick 2.9
import Lomiri.Components 1.3
import QtQuick.Layouts 1.1
import QtGraphicalEffects 1.0

import "../generalfunctions.js" as GeneralFunctions

RowLayout {
    id: platformLabel

    spacing: units.gu(1)
    Layout.fillWidth: false
    Layout.fillHeight: false
    Layout.alignment: Qt.AlignRight

    property var platform: null
    property bool alignRight: false

    visible: platform ? true : false

    Label {
        text: {
            if(!platform) {
                return "";
            }
            switch(platform.desc) {
                case "nástupiště/kolej":
                    return i18n.tr("platform/track");
                case "nástupiště":
                    return i18n.tr("platform");
                default:
                    return platform.desc;
            }
        }
        font.pixelSize: FontUtils.sizeToPixels("x-small")
        font.bold: false
        wrapMode: Text.Wrap
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        Layout.fillWidth: false

        visible: platformLabel.alignRight
    }
  
    Item {
        Layout.preferredWidth: platformLabelText.contentWidth
        Layout.preferredHeight: platformLabelText.contentHeight

        Label {
            id: platformLabelText
            anchors.centerIn: parent
            text: platform ? platform.text : ""
            font.pixelSize: FontUtils.sizeToPixels("normal")
            font.bold: true
            color: pageLayout.headerColor || pageLayout.colorPalete["headerBG"]
            wrapMode: Text.Wrap
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
        }

        DropShadow {
            anchors.fill: platformLabelText
            source: platformLabelText
            verticalOffset: 0
            radius: 2
            samples: 2
            spread: 0.7
            color: GeneralFunctions.invertColor(pageLayout.headerColor || pageLayout.colorPalete["headerBG"], true)
        }
    }
}

