import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0

import "../transport-api.js" as Transport

Item {
    id: colorPicker
    anchors {
        left: parent.left
        right: parent.right
        topMargin: units.gu(2)
        bottomMargin: units.gu(2)
    }
    height: childrenRect.height + units.gu(5)
    
    property var transportType: null
    property var selectedIndex: -1
    property var selectChangeCallback: null;
    
    function append(itemData) {
        rowListModel.append(itemData);
    }
    
    function setTransportType(id) {
        transportType = id;
        setCurrentColor();
    }
    
    function callOnSelectChange(callback) {
        selectChangeCallback = callback;
    }
    
    function applyNewColor(color) {
        if(transportType) {
            Transport.transportOptions.saveDBSetting(transportType, color);
            var newColor = color || pageLayout.colorPalete.headerBG;
            wrappingRectangle.color = newColor;
            return newColor;
        }
        return pageLayout.colorPalete.headerBG;
    }
    
    function getCurrentColor() {
        return Transport.transportOptions.getDBSetting(transportType) || pageLayout.colorPalete.headerBG;
    }
    
    function setCurrentColor() {
        if(transportType) {
            wrappingRectangle.color = getCurrentColor();
        }
    }
    
    Component {
        id: rowButton

        Rectangle {
            width: rowButtonsLayout.width / rowListModel.count
            height: units.gu(4)
            color: colorValue ? colorValue : pageLayout.colorPalete.headerBG
            opacity: index === selectedIndex ? 1 : 0.5
            
            Image {
                id: resetIcon
                anchors {
                    fill: parent
                    margins: parent.width / 8
                }
                source: Qt.resolvedUrl("qrc:/images/reset.svg")
                sourceSize: Qt.size(width, height)
                fillMode: Image.PreserveAspectFit
                visible: false
            }
            
            ColorOverlay {
                anchors.fill: resetIcon
                source: resetIcon
                color: "#ffffffff"
                cached: true
                visible: !colorValue
            }
            
            MouseArea {
                anchors.fill: parent                
                onClicked: {
                    var newColor = applyNewColor(colorValue);
                    colorPicker.selectedIndex = index;
                    if(colorPicker.selectChangeCallback) {
                        colorPicker.selectChangeCallback(newColor);
                    }
                }
            }
            
            Component.onCompleted: {
                if(Transport.transportOptions.getDBSetting(colorPicker.transportType) === colorValue) {
                    colorPicker.selectedIndex = index;
                }
            }
        }
    }
    
    Rectangle {
        id: wrappingRectangle
        anchors {
            left: parent.left
            right: parent.right
            verticalCenter: parent.verticalCenter
        }
        height: childrenRect.height
        color: "transparent"
        
        ColumnLayout {
            anchors {
                left: parent.left
                right: parent.right
            }
            spacing: units.gu(2)
        
            ListView {
                id: rowButtonsLayout
                Layout.fillWidth: true
                model: ListModel {
                    id: rowListModel
                }
                spacing: 0
                interactive: false
                orientation: Qt.Horizontal
                delegate: rowButton
            }
        }
    }
}
