"use strict";

/* Single Event structure */
var Event = function(data) {
    data = data || {};
    this.name = data.name || null;
    this.response = data.response || null;
    this.listeners = [];
    this.lastResult = null;
}

Event.prototype.update = function(data) {
    if(typeof data.response !== typeof undefined) {
        if(typeof data.response === "function") {
            this.lastResult = data.response();
        }
        else {
            this.lastResult = data.response;
        }
    }
    
    var actionResponses = [];
    for(var i = 0; i < this.listeners.length; i++) {
        const reply = this.listeners[i].callback(this.lastResult);
        
        actionResponses.push({
            name: this.name,
            description: this.listeners[i].description || null,
            result: this.lastResult,
            reply: reply
        });
    }
    return actionResponses;
}

Event.prototype.listen = function(data) {
    var callback = data.callback || null;
    if(callback) {
        for(var i = 0; i < this.listeners.length; i++) {
            if(this.listeners[i].callback === callback) {
                return null;
            }
        }
        
        this.listeners.push({
            description: data.description || null,
            callback: callback
        });
        
        if(this.lastResult) {
            callback(this.lastResult);
        }
        
        return true;
    }
    return false;
}


/* Event collection structure */
var EventListener = function(data) {
    data = data || {};
    this.ghostEvents = {};
    this.registeredEvents = [];
    
    return this;
}

EventListener.prototype.getKeyValue = function(data, key) {
    return data && data[key] ? data[key] : null;
}

EventListener.prototype.getEventByName = function(eventName) {
    for(var i = 0; i < this.registeredEvents.length; i++) {
        if(this.registeredEvents[i].name === eventName) {
            return this.registeredEvents[i];
        }
    }
    return false;
}

EventListener.prototype.waitForEvent = function(data) {
    const name = this.getKeyValue(data, "name");
    this.ghostEvents[name] = this.ghostEvents[name] || [];
    this.ghostEvents[this.getKeyValue(data, "name")].push(data);
}

EventListener.prototype.getUnassignedListeners = function(name) {
    return this.ghostEvents[name] ? this.ghostEvents[name] : [];
}

EventListener.prototype.deleteAllListeners = function(name) {
    delete this.ghostEvents[name];
}

EventListener.prototype.attachUnassignedListenersToEvent = function(event) {
    const listeners = this.getUnassignedListeners(event.name);
    var listenersLength = listeners.length;
    for(var i = 0; i < listenersLength; i++) {
        event.listen(listeners[i]);
    }
    this.deleteAllListeners(name);
    return listenersLength;
}

EventListener.prototype.createEvent = function(data) {
    var name = data.name || null;
    if(name) {
        var event = this.getEventByName(name);
        if(event) {
            return event;
        }
        
        var newEvent = new Event(data);
        if(this.ghostEvents[newEvent.name]) {
            this.attachUnassignedListenersToEvent(newEvent);
        }
        
        this.registeredEvents.push(newEvent);
        return newEvent;
    }
    return false;
}

EventListener.prototype.update = function(data) {
    var event = this.getEventByName(this.getKeyValue(data, "name")) || this.createEvent(data);
    if(event) {
        return event.update(data);
    }
    return false;
}

EventListener.prototype.listen = function(data) {
    var event = this.getEventByName(this.getKeyValue(data, "name")) || this.createEvent(data);
    if(event) {
        event.listen(data);
    }
}
