import QtQuick 2.9
import Lomiri.Components 1.3
import Lomiri.Content 1.3
import "../components"

import "../transport-api.js" as Transport

Page {
    id: sharePage
    property var activeTransfer
    property string string

    header: PageHeader {
        id: pageHeader
        title: i18n.tr("Share with")

        leadingActionBar {
            id: leadActions
            actions: [
                Action {
                    text: i18n.tr("Go back")
                    iconName: "back"
                    onTriggered: {
                        pageLayout.removePages(sharePage);
                    }
                }
            ]
            numberOfSlots: 1
        }

        StyleHints {
            foregroundColor: pageLayout.colorPalete["headerText"]
            backgroundColor: pageLayout.headerColor || pageLayout.colorPalete["headerBG"]
        }
    }

    clip: true

    ContentPeerPicker {
        anchors {
            fill: parent
            topMargin: sharePage.header.height
        }
        showTitle: false
        contentType: ContentType.Text
        handler: ContentHandler.Share

        onPeerSelected: {
            sharePage.activeTransfer = peer.request();

            sharePage.activeTransfer.items = [resultComponent.createObject(parent, {url: "", name: "Transport export", "text": sharePage.string || ""})];
            sharePage.activeTransfer.state = ContentTransfer.Charged;

            pageLayout.removePages(sharePage);
        }

        onCancelPressed: {
            pageLayout.removePages(sharePage);
        }
    }

    Component {
        id: resultComponent
        ContentItem {}
    }
}
