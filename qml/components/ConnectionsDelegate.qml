import QtQuick 2.9
import Lomiri.Components 1.3
import QtQuick.Layouts 1.1
import Lomiri.Content 1.3

import "../generalfunctions.js" as GeneralFunctions

Component {
    id: connectionsDelegate

    ListItem {
        id: connectionsDelegateItem
        anchors {
            left: parent.left
            right: parent.right
        }
        height: connectionsDelegateItemRectangle.height + 2 * connectionsDelegateItemRectangle.anchors.margins
        divider.visible: true

        property var connection: null
        property var detail: null

        function openConnectionDetail() {
            if(connectionsDelegateItem.connection) {
                if(connectionsDelegateItem.detail !== null) {
                    connectionDetailPage.renderDetail(connectionsDelegateItem.detail.getConnectionDetail());
                    pageLayout.addPageToCurrentColumn(connectionsPage, connectionDetailPage);
                    connectionsDelegateItem.state = "complete";
                }
                else if(connectionsDelegateItem.state !== "loading") {
                    connectionsDelegateItem.state = "loading";
                    connectionsDelegateItem.connection.getDetail(function(object, state) {
                        if(state === "SUCCESS") {
                            if(object) {
                                connectionsDelegateItem.detail = object;
                                connectionDetailPage.renderDetail(object.getConnectionDetail());
                                pageLayout.addPageToCurrentColumn(connectionsPage, connectionDetailPage);
                                connectionsDelegateItem.state = "complete";
                            }
                            else {
                                connectionsDelegateItem.state = "empty";
                                errorMessage.value = i18n.tr("Could not load connection detail");
                            }
                        }
                        else if(state !== "ABORT") {
                            connectionsDelegateItem.state = "empty";
                            errorMessage.value = i18n.tr("Could not load connection detail");
                        }
                    }, true /* Force update */);
                }
            }
        }
        
        function getTextContent() {
            var clipboardText = "";
            for(var i = 0; i < routesModel.count; i++) {
                var route = routesModel.get(i);
                clipboardText += route.num + " (" + route.type + ")\n";
                clipboardText += " - " + route.from.name + " " + route.from.time + "\n";
                clipboardText += " - " + route.to.name + " " + route.to.time + (i < routesModel.count - 1 ? "\n" : "");
            }
            return clipboardText;
        }

        states: [
            State {
                name: "empty"
                PropertyChanges { target: itemActivity; running: false }
                PropertyChanges { target: connectionsDelegateItem; }
            },
            State {
                name: "complete"
                PropertyChanges { target: itemActivity; running: false }
                PropertyChanges { target: connectionsDelegateItem; color: pageLayout.colorPalete["secondaryBG"] }
            },
            State {
                name: "loading"
                PropertyChanges { target: itemActivity; running: true }
                PropertyChanges { target: connectionsDelegateItem; color: Qt.darker(pageLayout.colorPalete["secondaryBG"], 1.1) }
            }
        ]

        state: "empty"

        trailingActions: ListItemActions {
            actions: [
                Action {
                    iconName: "share"
                    onTriggered: {
                        var sharePage = pageLayout.addPageToCurrentColumn(connectionsPage, Qt.resolvedUrl("../pages/SharePage.qml"), {"string": connectionsDelegateItem.getTextContent()});
                    }
                },
                Action {
                    iconName: "edit-copy"
                    onTriggered: {
                        Clipboard.push(connectionsDelegateItem.getTextContent() + "\n");
                    }
                },
                Action {
                    iconName: "view-expand"
                    onTriggered: {
                        if(progressLine.state !== "running") {
                            connectionsDelegateItem.openConnectionDetail();
                        }
                    }
                }
            ]
        }

        ListItemActions {
            id: leadingActionsComponent
            actions: [
                Action {
                    iconName: connectionsDelegateItem.state === "complete" ? "clear" : "cancel"
                    visible: connectionsDelegateItem.state !== "empty"
                    onTriggered: {
                        if(connectionsDelegateItem.state === "loading") {
                            connectionsDelegateItem.connection.abort();
                            connectionsDelegateItem.state = "empty";
                        }
                        else {
                            connectionsDelegateItem.detail = null;
                            connectionsDelegateItem.state = "empty";
                        }
                        connectionsDelegateItem.detail = null;
                        connectionsDelegateItem.connection.deleteDetail(); // Clear even the data, not just bubble color as users may want to refresh the data such as delay since these are also shown since Jan 22 2020.
                    }
                }
            ]
        }

        leadingActions: connectionsDelegateItem.state === "complete" ? leadingActionsComponent : null

        Rectangle {
            anchors {
                fill: parent
            }
            color: pageLayout.colorPalete["secondaryBG"]
            opacity: 0.75
            z: 1
            visible: itemActivity.running

            ActivityIndicator {
                id: itemActivity
                anchors.centerIn: parent
                running: false
            }
        }

        Item {
            id: connectionsDelegateItemRectangle
            anchors {
                left: parent.left
                right: parent.right
                margins: units.gu(2)
            }
            height: connectionColumn.height + 2 * anchors.margins

            Column {
                id: connectionColumn
                anchors {
                    left: parent.left
                    right: parent.right
                    verticalCenter: parent.verticalCenter
                }
                height: connectionRow.height + routesView.height
                spacing: units.gu(2)

                RowLayout {
                    id: connectionRow
                    anchors {
                        left: parent.left
                        right: parent.right
                    }
                    spacing: units.gu(2)

                    Label {
                        id: timeToLabel
                        text: "N/A"
                        font.pixelSize: FontUtils.sizeToPixels("normal")
                        font.bold: true
                        horizontalAlignment: Text.AlignLeft
                        wrapMode: Text.WordWrap

                        Layout.fillWidth: true
                        Layout.alignment: Qt.AlignLeft

                        property var departureTime: null
                        property var arrivalTime: null
                    }

                    Label {
                        id: timeLengthLabel
                        text: timeLength
                        font.pixelSize: FontUtils.sizeToPixels("normal")
                        font.bold: false
                        horizontalAlignment: Text.AlignRight
                        wrapMode: Text.WordWrap

                        Layout.fillWidth: true
                        Layout.alignment: Qt.AlignRight
                    }
                }

                ListView {
                    id: routesView
                    anchors {
                        left: parent.left
                        right: parent.right
                    }
                    interactive: false
                    height: childrenRect.height
                    delegate: routesDelegate
                    spacing: units.gu(1)

                    model: ListModel {
                        id: routesModel
                        property var firstDelay: 0
                        
                        onFirstDelayChanged: {
                            clockTimer.restart();
                        }
                    }

                    Component.onCompleted: {
                        connectionsDelegateItem.connection = connectionsModel.childModel[index];
                        const trains = connectionsDelegateItem.connection.trains;
                        const details = (trains || []).map((train, index) => {
                            const trainData = train.trainData || {};
                            const trainInfo = trainData.info || {}; 

                            const detail = {};
                            if(index === 0) {
                                timeToLabel.departureTime = GeneralFunctions.dateStringtoDate(train.dateTime1);
                            }
                            else if(index === trains.length - 1) {
                                timeToLabel.arrivalTime = GeneralFunctions.dateStringtoDate(train.dateTime2);
                            }

                            detail.num = trainInfo.num1 || "";
                            detail.name = trainInfo.num2 || "";
                            detail.type = trainInfo.type || "";
                            detail.typeName = trainInfo.typeName || "";
                            detail.typeIndex = trainInfo.id || 0;
                            detail.lineColor = GeneralFunctions.lineColor(detail.typeIndex, detail.num);
                            detail.fixedCodes = JSON.stringify(trainInfo.fixedCodes || []);
                            detail.delay = Number(train.delay) > 0 ? train.delay : 0;
                            detail.delayQuery = train.delayQuery || null;
                            detail.connectionsID = connectionsDelegateItem.connection.parent.id || null;

                            (trainData.route || []).forEach((station, index) => {
                                const platform = station.fixedCodes ? station.fixedCodes.find(({ desc }) => desc.includes("nástupiště")) : null;
                                if(index % 2 === 0) {
                                    detail.from = {
                                        name: station.station.name,
                                        time: station.depTime,
                                        fixedCodes: station.fixedCodes,
                                        desc: station.station.fixedCodes || [],
                                        platform
                                    };
                                }
                                else {
                                    detail.to = {
                                        name: station.station.name,
                                        time: station.arrTime || station.depTime,
                                        fixedCodes: station.fixedCodes,
                                        desc: station.station.fixedCodes || [],
                                        platform
                                    };
                                }
                            });
                            
                            if(index === 0) {
                                routesModel.firstDelay = detail.delay;
                            }

                            return detail;
                        }).forEach(detail => routesModel.append(detail));

                        if(connectionsDelegateItem.connection && connectionsDelegateItem.connection.detail !== null) {
                            connectionsDelegateItem.detail = connectionsDelegateItem.connection; // Passing the whole object
                            connectionsDelegateItem.state = "complete";
                        }
                    }
                }
            }
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(progressLine.state !== "running") {
                    connectionsDelegateItem.openConnectionDetail();
                }
            }
        }

        Timer {
            id: clockTimer
            running: parent.visible
            triggeredOnStart: true
            interval: 1000
            repeat: true
            onTriggered: {
                if(timeToLabel.departureTime) {
                    var departureTimePlusDelay = new Date(timeToLabel.departureTime.getTime() + (routesModel.firstDelay * 60000));
                    
                    var result = GeneralFunctions.remainingTimeLabel(departureTimePlusDelay);
                    timeToLabel.text = result.text;
                    if(result.inFuture) {
                        timeToLabel.color = pageLayout.colorPalete["successText"];
                    }
                    else {
                        timeToLabel.color = pageLayout.colorPalete["warningText"];
                        repeat = false;
                    }
                }
            }
        }
    }
}
