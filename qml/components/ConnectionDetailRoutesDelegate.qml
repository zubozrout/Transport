import QtQuick 2.9
import Lomiri.Components 1.3
import QtQuick.Layouts 1.1
import QtGraphicalEffects 1.0

import "../generalfunctions.js" as GeneralFunctions

Component {
    id: connectionDetailRoutesDelegate

    Rectangle {
        anchors {
            left: parent.left
            right: parent.right
        }
        visible: !stopPassed && connectionDetailSections.selectedIndex === 0 ? false : true
        height: visible ? stationColumn.height : 0
        color: index % 2 === 0 ? "transparent" : pageLayout.colorPalete["secondaryBG"]

        Row {
            id: stationColumn
            anchors {
                left: parent.left
                right: parent.right
                leftMargin: units.gu(2)
                rightMargin: units.gu(2)
            }
            spacing: units.gu(1)

            Rectangle {
                id: positionIndicator
                anchors.verticalCenter: parent.verticalCenter
                width: units.gu(1.5)
                height: width
                color: pageLayout.headerColor
                radius: width
                opacity: 0

                Timer {
                    repeat: true
                    interval: 5000
                    triggeredOnStart: true
                    running: visible
                    onTriggered: {
                        if(checkStationPassed()) {
                            positionIndicator.opacity = 1;
                        }
                        else {
                            positionIndicator.opacity = 0;
                        }
                    }

                    function checkStationPassed() {
                        const currentDate = new Date();
                        const currentDateShifted = new Date(currentDate.setMinutes(currentDate.getMinutes() - GeneralFunctions.cachedDateCETOffset));
                        currentDateShifted.setMilliseconds(0);
                        currentDateShifted.setSeconds(0);

                        currentDateShifted.setMinutes(currentDateShifted.getMinutes() - delay);
                        if(arrTime) {
                            if(new Date(arrTime) <= currentDateShifted) {
                                return true;
                            }
                            return false;
                        }
                        if(depTime) {
                            if(new Date(depTime) <= currentDateShifted) {
                                return true;
                            }
                            return false;
                        }
                    }
                }
            }

            RowLayout {
                width: parent.width/2 - positionIndicator.width - stationColumn.spacing
                spacing: units.gu(1)

                Label {
                    id: stationNameLabel
                    Layout.preferredWidth: parent.width - units.gu(6)
                    Layout.fillWidth: true
                    
                    text: station.name || ""
                    font.pixelSize: FontUtils.sizeToPixels("normal")
                    font.bold: stopPassed ? true : false
                    horizontalAlignment: Text.AlignLeft
                    wrapMode: Text.Wrap
                }

                // Stop on demand
                Item {
                    Layout.fillWidth: false
                    Layout.preferredWidth: demandIcon.visible ? Layout.preferredHeight : 0
                    Layout.preferredHeight: stationNameLabel.font.pixelSize

                    Image {
                        id: demandIcon
                        anchors.fill: parent
                        
                        source: Qt.resolvedUrl("qrc:/icons/stop-bell.svg")
                        fillMode: Image.PreserveAspectFit
                        
                        visible: fixedCodes.split(";").find(fixedCode => fixedCode === "x") || false
                    }

                    ColorOverlay {
                        anchors.fill: demandIcon
                        source: demandIcon
                        color: pageLayout.colorPalete["baseText"]
                        cached: true
                    }
                }

                // Stop with a subway transfer
                Item {
                    Layout.fillWidth: false
                    Layout.preferredWidth: metroIcon.visible ? Layout.preferredHeight : 0
                    Layout.preferredHeight: stationNameLabel.font.pixelSize

                    Image {
                        id: metroIcon
                        anchors.fill: parent
                        
                        source: Qt.resolvedUrl("qrc:/icons/stop-metro.svg")
                        fillMode: Image.PreserveAspectFit
                        
                        visible: fixedCodes.split(";").find(fixedCode => fixedCode.match(/^M\d$/i) || fixedCode.includes("-M-")) || false
                    }

                    ColorOverlay {
                        anchors.fill: metroIcon
                        source: metroIcon
                        color: pageLayout.colorPalete["baseText"]
                        cached: true
                    }
                }
                
                Item {
                    Layout.fillWidth: true
                }
            }

            Label {
                text: GeneralFunctions.dateToTimeString(arrTime)
                font.pixelSize: FontUtils.sizeToPixels("normal")
                font.bold: stopPassed ? true : false
                horizontalAlignment: Text.AlignLeft
                wrapMode: Text.WordWrap
                width: parent.width/4 - parent.spacing
            }

            Label {
                text: GeneralFunctions.dateToTimeString(depTime)
                font.pixelSize: FontUtils.sizeToPixels("normal")
                font.bold: stopPassed ? true : false
                horizontalAlignment: Text.AlignLeft
                wrapMode: Text.WordWrap
                width: parent.width/4 - parent.spacing
            }            
        }
    }
}
