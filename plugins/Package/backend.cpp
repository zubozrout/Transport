#include <QtQml>
#include <QtQml/QQmlContext>
#include "backend.h"
#include "package.h"


void BackendPlugin::registerTypes(const char *uri) {
    Q_ASSERT(uri == QLatin1String("Transport"));
    qmlRegisterType<Package>(uri, 1, 0, "Package");
}

void BackendPlugin::initializeEngine(QQmlEngine *engine, const char *uri) {
    QQmlExtensionPlugin::initializeEngine(engine, uri);
}

