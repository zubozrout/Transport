"use strict";

var ConnectionDetail = function(data) {
    this.data = data || {};
    this.connectionsID = this.data.connectionsID || null;
    this.id = this.data.id || 0;
    this.distance = this.data.distance || 0;
    this.timeLength = this.data.timeLength || 0;
    this.price = this.data.price || 0;
    this.trains = this.data.trains || [];
    
    this.parsedTrains = [];
    this.parseTrains();

    return this;
}

ConnectionDetail.prototype.trainInfo = function(trainData) {
    trainData = trainData || {};
    var info = trainData.info;
    var trainInfo = {};
    trainInfo.train = info.train || 0;
    trainInfo.num = info.num1 || 0;
    trainInfo.name = info.num2 || "";
    trainInfo.type = info.type || "";
    trainInfo.typeName = info.typeName || "";
    trainInfo.color = info.color || "00000000";
    trainInfo.id = info.id || 0;
    trainInfo.fixedCodes = info.fixedCodes || [];
    return trainInfo;
}

ConnectionDetail.prototype.station = function(station) {
    station = station || {};
    var stationData = {};
    stationData.station = station.station || 0;
    stationData.name = station.name || "";
    stationData.key = station.key || "";
    stationData.coorX = station.coorX || "";
    stationData.coorY = station.coorY || "";
    return stationData;
}

ConnectionDetail.prototype.stop = function(stop, dateArrCopy, dateDepCopy) {
    stop = stop || {};

    var arrTime = dateArrCopy || "";
    var depTime = dateDepCopy || "";

    var stopData = {};
    stopData.station = this.station(stop.station);
    stopData.arrTime = stop.arrTime ? arrTime.toString() : "";
    stopData.depTime = stop.depTime ? depTime.toString() : "";
    stopData.dist = stop.dist || "";
    return {
        stopData: stopData,
        arrTime: arrTime,
        depTime: depTime
    };
}

ConnectionDetail.prototype.checkStops = function(data) {
    data = data || {};

    const route = data.route || [];
    const routeDateDep = data.dateTime1 || new Date(); // Route departure
    const routeDateArr = data.dateTime2 || new Date(); // Route arrival
    const from = data.from || 0; // Route start
    const to = data.to || route.length - 1; // Route end

    const stops = [];
    if(route) {
        let prevStep = null;
        route.forEach((routeStep, index) => {
            routeStep.routeStarted = index >= from;
            routeStep.routeEnded = index > to;
            
            const timeStringToDate = this.timeStringToDate; /* Weird QML workaround to avoid this being undefined in nested if statement */
            if(routeStep.arrTime) {
                routeStep.dateArr = timeStringToDate(routeStep.arrTime, prevStep ? (prevStep.dateArr || prevStep.dateDep) : routeDateDep);
            }
            if(routeStep.depTime) {
                routeStep.dateDep = timeStringToDate(routeStep.depTime, prevStep ? (prevStep.dateDep || prevStep.dateArr) : routeDateDep);
            }
            
            prevStep = routeStep;
        });
        
        /* 
         * We consider a date with lower time a new day as there is the api doesn't provide the necessary info
         * and we presume no transport takes more than 23:59 in between two stops.
         */
        const isNewDay = (step, copmarisonStep) => {
            if(step && copmarisonStep) {
                if(step.dateArr) {
                    if(copmarisonStep.dateArr) {
                        return step.dateArr < copmarisonStep.dateArr;
                    }
                    if(copmarisonStep.dateDep) {
                        return step.dateArr < copmarisonStep.dateDep;
                    }
                }
                if(step.dateDep) {
                    if(copmarisonStep.dateArr) {
                        return step.dateDep < copmarisonStep.dateArr;
                    }
                    if(copmarisonStep.dateDep) {
                        return step.dateDep < copmarisonStep.dateDep;
                    }
                }
            }
            return false;
        };
        
        const changeDay = (step, increment) => {
            step.dateArr && step.dateArr.setDate(step.dateArr.getDate() + (increment || 1));
            step.dateDep && step.dateDep.setDate(step.dateDep.getDate() + (increment || 1));
        };
        
        /* Increment dates as they go on since route started */
        prevStep = null;
        route.filter(routeStep => routeStep.routeStarted).forEach((routeStep, index) => {
            if(isNewDay(routeStep, prevStep)) {
                changeDay(routeStep, 1);
            }
            prevStep = routeStep;
        });
        
        /* Decrement dates as they go on from route started backwards */
        prevStep = route[from];
        route.filter(routeStep => !routeStep.routeStarted ).reverse().forEach((routeStep, index) => {
            if(isNewDay(prevStep, routeStep)) {
                changeDay(routeStep, -1);
            }
            prevStep = routeStep;
        });
        
        /* Compose */
        route.forEach((routeStep, index) => {
            const stop = this.stop(routeStep, routeStep.dateArr, routeStep.dateDep);
            stop.stopData.routeStarted = routeStep.routeStarted;
            stop.stopData.routeEnded = routeStep.routeEnded;
            stop.stopData.stopPassed = routeStep.routeStarted && !routeStep.routeEnded;
            
            const fixedCodes = routeStep.fixedCodes || routeStep.station.fixedCodes || [];
            stop.stopData.fixedCodes = fixedCodes.map(fixedCode => fixedCode.text).join(";");
            stops.push(stop.stopData);
        });
    }
    return stops;
}

ConnectionDetail.prototype.trainRoute = function(data) {
    return this.checkStops(data);
}

ConnectionDetail.prototype.trainRouteCoors = function(data) {
    var route = data.route || [];
    var paths = [];
    for(var i = 0; i < route.length; i++) {
        var path = {};
        path.coorX = route[i].coorX;
        path.coorY = route[i].coorY;
        path.active = i >= (data.from || 0) && i < (data.to || route.length);
        paths.push(path);
    }
    return paths;
}

ConnectionDetail.prototype.parseTrainDetail = function(train) {
    train = train || {};
    var detailData = {};
    detailData.connectionsID = this.connectionsID || null;
    detailData.from = train.from || "";
    detailData.to = train.to || "";
    detailData.dateTime1 = train.dateTime1 ? dateStringtoDate(String(train.dateTime1)) : "";
    detailData.dateTime2 = train.dateTime2 ? dateStringtoDate(String(train.dateTime2)) : "";
    detailData.distance = train.distance || "";
    detailData.timeLength = train.timeLength || "";
    detailData.delay = Number(train.delay) > 0 ? Number(train.delay) : 0;
    detailData.delayQuery = train.delayQuery || "";
    detailData.stdChange = train.stdChange || 0;
    detailData.from = train.from || 0;
    detailData.to = train.to || (train.trainData ? train.trainData.route.length : 0)
    detailData.trainInfo = this.trainInfo(train.trainData);
    detailData.route = this.trainRoute({
        route: train.trainData.route,
        dateTime1: detailData.dateTime1,
        dateTime2: detailData.dateTime2,
        from: train.from,
        to: train.to
    });
    detailData.routeCoors = this.trainRouteCoors({
        route: train.trainData.route,
        from: train.from,
        to: train.to
    });
    return detailData;
}

ConnectionDetail.prototype.parseTrains = function() {
    for(var i = 0; i < this.trains.length; i++) {
        this.parsedTrains.push(this.parseTrainDetail(this.trains[i]));
    }
}

ConnectionDetail.prototype.trainLength = function(index) {
    return this.parsedTrains.length;
}

ConnectionDetail.prototype.getTrain = function(index) {
    if(index >= 0 && index < this.parsedTrains.length) {
        return this.parsedTrains[index];
    }
    return null;
}

ConnectionDetail.prototype.timeStringToDate = function(timeString, date) {
    if(timeString && date) {
        var dateCopy = new Date(date);
        var timeParts = timeString.replace(/^24:0+$/, "00:00").split(":"); /* Replace 24:00 which api might return with 0:00 as the actually mean 0:00 afaik */
        dateCopy.setHours(timeParts[0], timeParts[1], 0, 0);
        return dateCopy;
    }
    return null;
}

ConnectionDetail.prototype.toString = function() {
    return JSON.stringify(this.data);
}

ConnectionDetail.prototype.whoAmI = function() {
    return "ConnectionDetail";
}
