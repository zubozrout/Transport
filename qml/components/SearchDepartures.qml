import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import Lomiri.Components 1.3 as UITK

import "../transport-api.js" as Transport
import "../generalfunctions.js" as GeneralFunctions

Item {
    id: searchDeparturesContainer
        
    function search() {
        var dateTime = null;
        if(dateTimeSelector.customDate) {
            dateTime = dateTimeSelector.dateTime;
        }
        
        Transport.support.searchDepartures({
            TransportStop: Transport.Stop,
            selectedTransport: Transport.transportOptions.getSelectedTransport(),
            stop: stop.value,
            isDep: true,
            dateTime: dateTime,
            limit: sliderCount.value
        });

        const stopName = stop.stopData ? stop.stopData.getName() : null;
        stopName && Transport.transportOptions.dbConnection.saveSetting("departures-last-stop", stopName);
    }
    
    Component.onCompleted: {
        /* Init */
        Transport.eventListener.listen({
            name: "fetching-departures-success",
            callback: function(data) {
                if(data) {
                    departuresPage.loadList({
                        departures: data
                    });
                    pageLayout.addPageToNextColumn(searchPage, departuresPage);
                }
            }
        });
    }
    
    Flickable {
        id: searchDeparturesFlickable
        anchors.fill: parent
        contentWidth: width
        contentHeight: searchColumn.height + 2 * searchColumn.anchors.margins
        flickableDirection: Flickable.VerticalFlick
        interactive: contentHeight > height
        clip: true
        
        Column {
            id: searchColumn
            anchors {
                left: parent.left
                right: parent.right
                margins: units.gu(1)
            }
            
            spacing: units.gu(2)

            StopSearch {
                id: stop
                z: 10

                property var searchFunction: searchDeparturesContainer.search
                
                Component.onCompleted: {
                    Transport.eventListener.listen({
                        name: "transport-change",
                        callback: selectedTransport => {
                            const previousSearch = Transport.transportOptions.dbConnection.getSetting("departures-last-stop");
                            if(previousSearch && previousSearch.length) {
                                stop.setData({ value: previousSearch });
                                return;
                            }

                            const hasMatch = (Transport.transportOptions.dbConnection.getSearchHistory() || []).find(({ key, stopidfrom, stopnamefrom }) => {
                                if(key === selectedTransport.getId()) {
                                    if(stopidfrom >= 0 && stopnamefrom) {
                                        GeneralFunctions.setStopData({
                                            Stop: Transport.Stop,
                                            dbConnection: selectedTransport.dbConnection,
                                            stopSearch: stop,
                                            stopidfrom: stopidfrom,
                                            stopnamefrom: stopnamefrom,
                                            typeid: key
                                        });
                                        return true;
                                    }
                                }
                                return false;
                            });
                            !hasMatch && empty();
                        }
                    });
                }
            }
            
            DateTimeSelector {
                id: dateTimeSelector
            }
            
            RowLayout {
                width: parent.width
                spacing: units.gu(2)
                
                Label {
                    text: i18n.tr("Count:") + " " + sliderCount.value
                    wrapMode: Text.WordWrap
                    horizontalAlignment: Text.AlignLeft
                    color: pageLayout.colorPalete["baseText"]
                    Layout.fillWidth: false
                    Layout.maximumWidth: parent.width / 2
                    Layout.alignment: Qt.AlignVCenter
                }
                
                Slider {
                    id: sliderCount
                    from: 10
                    to: 50
                    stepSize: 5
                    value: Number(Transport.transportOptions.getDBSetting("number-of-departures-results") || 10);
                    snapMode: Slider.SnapAlways
                    Layout.fillWidth: true
                    Layout.alignment: Qt.AlignVCenter
                    handle: Rectangle {
                        x: sliderCount.leftPadding + sliderCount.visualPosition * (sliderCount.availableWidth - width)
                        y: sliderCount.topPadding + sliderCount.availableHeight / 2 - height / 2
                        implicitWidth: units.gu(2)
                        implicitHeight: units.gu(2)
                        radius: units.gu(1)
                        border.color: pageLayout.colorPalete["baseText"]
                    }
                    
                    onValueChanged: {
                        Transport.transportOptions.saveDBSetting("number-of-departures-results", value);
                    }
                }
            }
            
            RectangleButton {
                id: rectangleButton
                anchors.horizontalCenter: parent.horizontalCenter
                text: itemActivity.running ? i18n.tr("Abort search") : i18n.tr("Search")
                enabled: transportOptionSelected && stop.value
                color: active ? pageLayout.colorPalete["headerBG"] : "#ddd"
                z: 1
                
                property bool transportOptionSelected: false

                UITK.ActivityIndicator {
                    id: itemActivity
                    anchors {
                        fill: parent
                        centerIn: parent
                        margins: parent.height/6
                    }
                    running: false
                    
                    Component.onCompleted: {
                        Transport.eventListener.listen({
                            name: "fetching-departures",
                            callback: function(value) {
                                if(value) {
                                    itemActivity.running = true;
                                }
                                else {
                                    itemActivity.running = false;
                                }
                            }
                        });
                    }
                }

                Component.onCompleted: {
                    setCallback(function() {
                        if(!itemActivity.running) {
                            searchDeparturesContainer.search();
                        }
                        else {
                            var selectedTransport = Transport.transportOptions.getSelectedTransport();
                            if(selectedTransport) {
                                selectedTransport.abortAll();
                            }
                        }
                    });
                    
                    Transport.eventListener.listen({
                        name: "transport-change",
                        callback: function(selectedTransport) {
                            transportOptionSelected = selectedTransport ? true : false;
                        }
                    });
                }
            }
        }
    }
    
    UITK.Scrollbar {
        id: scrollBar
        flickableItem: searchDeparturesFlickable
        align: Qt.AlignTrailing
    }
    
    Keys.onUpPressed: scrollBar.decrease()
    Keys.onDownPressed: scrollBar.increase()
}
