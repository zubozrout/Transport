"use strict";

const isCzechSite = window.location.href.match(/index_cs.html/);

if(!window.location.href.match(/(\?|&)lang/)) {
	if(navigator.language === "cs") {
		if(!isCzechSite) {
			window.location.replace("./index_cs.html");
		}
	}
	else {
		if(isCzechSite) {
			window.location.replace("./index.html");
		}
	}
}

document.addEventListener("DOMContentLoaded", (event) => {
	const lang = document.createElement("a");
	lang.classList.add("lang-switch");
	lang.innerText = isCzechSite ? "View in English" : "View in Czech";
	lang.href = (isCzechSite ? "." : "./index_cs.html") + "?lang";
	document.querySelector("header").appendChild(lang);
});
