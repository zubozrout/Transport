import QtQuick 2.9
import Lomiri.Components 1.3
import QtQuick.LocalStorage 2.0
import QtQuick.Layouts 1.2
import Lomiri.Components.Popups 1.3

import "../transport-api.js" as Transport

Component {
    id: transportDelegate
    
    ListItem {
        id: transportDelegateItem
        width: parent.width
        height: transportDelegateRectangle.height + 2 * transportDelegateRectangle.anchors.margins
        divider.visible: true
        color: selected || transportSelectorPage.editColors ? bgColor : "transparent"
        highlightColor: bgColor
        swipeEnabled: !transportSelectorPage.editColors

        onVisibleChanged: {
            selected = Transport.transportOptions.getSelectedId() === id;
        }
        
        property var expanded: false
        property var bgColor: pageLayout.colorPalete.headerBG
        property var textColor: selected || highlighted || transportSelectorPage.editColors ? Qt.rgba(255, 255, 255, 0.85) : transportDelegateItem.bgColor

        trailingActions: ListItemActions {
            actions: [
                Action {
                    iconName: expanded ? "view-collapse" : "view-expand"
                    onTriggered: {
                        expanded = !expanded;
                    }
                },
                Action {
                    iconName: "delete"
                    onTriggered: PopupUtils.open(confirmStationStopRemoval)
                    visible: isUsed ? true : false
                }
            ]
        }

        Component {
            id: confirmStationStopRemoval

            Dialog {
                id: confirmStationStopRemovalDialogue
                title: i18n.tr("Attention")
                text: i18n.tr("Do you really want to delete all saved stops for %1 transport option?").arg((name ? name : ""))
                Button {
                    text: i18n.tr("No")
                    onClicked: PopupUtils.close(confirmStationStopRemovalDialogue)
                }
                Button {
                    text: i18n.tr("Yes")
                    color: LomiriColors.red
                    onClicked: {
                        Transport.transportOptions.dbConnection.clearStationsForId(id);
                        PopupUtils.close(confirmStationStopRemovalDialogue);
                        pageLayout.removePages(transportSelectorPage);
                    }
                }
            }
        }
        
        Rectangle {
            anchors.fill: parent
            color: "transparent"

            Rectangle {
                id: transportDelegateRectangle
                anchors {
                    left: parent.left
                    right: parent.right
                    margins: units.gu(2)
                    verticalCenter: parent.verticalCenter
                }
                height: childrenRect.height
                color: "transparent"

                Column {
                    id: transportDelegateColumn
                    width: parent.width

                    RowLayout {
                        spacing: units.gu(2)
                        width: parent.width

                        Icon {
                            name: "favorite-selected"
                            width: visible ? units.gu(2) : 0
                            color: transportDelegateItem.textColor
                            visible: isUsed ? true : false
                        }

                        Label {
                            text: nameExt
                            font.pixelSize: FontUtils.sizeToPixels("normal")
                            color: transportDelegateItem.textColor
                            wrapMode: Text.WordWrap

                            Layout.fillWidth: true
                        }
                    }
                    
                    ColorPicker {
                        id: colorPicker
                        visible: isUsed && transportSelectorPage.editColors
                        Component.onCompleted: {
                            if(isUsed) {
                                setTransportType(id);
                                append({colorValue: null});
                                append({colorValue: "#B71C1C"});
                                append({colorValue: "#880E4F"});
                                append({colorValue: "#4A148C"});
                                append({colorValue: "#311B92"});
                                append({colorValue: "#1A237E"});
                                append({colorValue: "#0D47A1"});
                                append({colorValue: "#01579B"});
                                append({colorValue: "#006064"});
                                append({colorValue: "#004D40"});
                                append({colorValue: "#1B5E20"});
                                append({colorValue: "#33691E"});
                                append({colorValue: "#827717"});
                                append({colorValue: "#F57F17"});
                                append({colorValue: "#FF6F00"});
                                append({colorValue: "#E65100"});
                                append({colorValue: "#BF360C"});
                                callOnSelectChange(function(selectedColor) {
                                    console.log("selectedColor", selectedColor);
                                    if(transportDelegateItem.selected) {
                                        pageLayout.setHeaderColor(selectedColor);
                                    }
                                    transportDelegateItem.bgColor = selectedColor;
                                });
                            }
                            
                            transportDelegateItem.bgColor = getCurrentColor();
                        }
                    }
                    
                    Item {
                        width: parent.width
                        height: childrenRect.height
                        visible: expanded && !transportSelectorPage.editColors

                        Column {
                            width: parent.width

                            Rectangle {
                                width: parent.width
                                height: units.gu(2)
                                color: "transparent"
                            }

                            Label {
                                text: name
                                width: parent.width
                                font.bold: true
                                wrapMode: Text.WordWrap
                                color: transportDelegateItem.textColor
                            }


                            Label {
                                text: title
                                width: parent.width
                                wrapMode: Text.WordWrap
                                visible: text !== nameExt.text
                                color: transportDelegateItem.textColor
                            }

                            Label {
                                text: typeof homeState !== typeof undefined ? homeState : ""
                                width: parent.width
                                font.bold: true
                                wrapMode: Text.WordWrap
                                visible: text !== ""
                                color: transportDelegateItem.textColor
                            }

                            GridLayout {
                                width: parent.width
                                columns: 2

                                Label {
                                    text: i18n.tr("City:") + " "
                                    wrapMode: Text.WordWrap
                                    visible: city !== ""
                                    color: transportDelegateItem.textColor
                                }

                                Label {
                                    text: city
                                    font.bold: true
                                    wrapMode: Text.WordWrap
                                    visible: city
                                    color: transportDelegateItem.textColor
                                    Layout.fillWidth: true
                                }

                                Label {
                                    text: i18n.tr("Valid from:") + " "
                                    wrapMode: Text.WordWrap
                                    visible: validFrom.text !== ""
                                    color: transportDelegateItem.textColor
                                }

                                Label {
                                    id: validFrom
                                    text: ttValidFrom
                                    wrapMode: Text.WordWrap
                                    visible: text !== ""
                                    color: transportDelegateItem.textColor
                                    Layout.fillWidth: true
                                }

                                Label {
                                    text: i18n.tr("Expires:") + " "
                                    wrapMode: Text.WordWrap
                                    visible: validTo.text !== ""
                                    color: transportDelegateItem.textColor
                                }

                                Label {
                                    id: validTo
                                    text: ttValidTo
                                    wrapMode: Text.WordWrap
                                    visible: text !== ""
                                    color: transportDelegateItem.textColor
                                    Layout.fillWidth: true
                                }

                                Label {
                                    text: i18n.tr("Vehicles:") + " "
                                    wrapMode: Text.WordWrap
                                    visible: trTypesList.text !== ""
                                    color: transportDelegateItem.textColor
                                }

                                Label {
                                    id: trTypesList
                                    text: trTypes ? trTypes : ""
                                    wrapMode: Text.WordWrap
                                    visible: text !== ""
                                    color: transportDelegateItem.textColor
                                    Layout.fillWidth: true
                                }
                            }
                        }
                    }
                }
            }
        }

        MouseArea {
            anchors.fill: parent
            enabled: !transportSelectorPage.editColors
            
            function composeAction() {
                Transport.transportOptions.selectTransportById(id);
                transportDelegateItem.expanded = false;
                pageLayout.removePages(transportSelectorPage);
            }
            
            onClicked: {
                Haptics.play();
                if(!transportDelegateItem.expanded) {
                    composeAction();
                }
            }
            
            onDoubleClicked: {
                composeAction();
            }
            
            onPressAndHold: {
                transportDelegateItem.expanded = !transportDelegateItem.expanded;
            }
        }
    }
}
