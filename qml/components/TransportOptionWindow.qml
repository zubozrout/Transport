import QtQuick 2.9
import QtQuick.Layouts 1.3
import Lomiri.Components 1.3

import "../transport-api.js" as Transport

Item {
    id: transportOptionWindow
    anchors {
        left: parent.left
        right: parent.right
        margins: units.gu(1)
    }
    height: transportOptionWindowRow.height + 2 * anchors.margins
    
    Row {
        id: transportOptionWindowRow
        anchors.verticalCenter: parent.verticalCenter
        width: parent.width
        height: units.gu(4)
        spacing: units.gu(2)

        Label {
            id: transportOptionLabel
            width: parent.width - searchButton.width - parent.spacing
            height: parent.height
            text: ""

            font.pixelSize: FontUtils.sizeToPixels("normal")
            font.bold: true
            wrapMode: Text.WordWrap
            verticalAlignment: Text.AlignVCenter

            property bool ok: false
            
            Component.onCompleted: {
                Transport.eventListener.listen({
                    name: "transport-change",
                    callback: function(selectedTransport) {
                        if(selectedTransport) {
                            ok = true;
                            text = selectedTransport.getName(Transport.langCode(true));
                        }
                        else {
                            ok = false;
                            text = i18n.tr("Select a transport type");
                        }
                    }
                });
            }
        }

        Button {
            id: searchButton
            width: units.gu(4)
            height: parent.height
            color: "transparent"

            onClicked: {
                pageLayout.addPageToNextColumn(searchPage, transportSelectorPage);
            }

            Icon {
                anchors.fill: parent
                name: "view-list-symbolic"
            }
        }
    }
}
